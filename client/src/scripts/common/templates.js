PS.templates["calendar"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<section type=\"calendar\" ps-sectionid=\""
    + escapeExpression(((helper = (helper = helpers.sectionid || (depth0 != null ? depth0.sectionid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"sectionid","hash":{},"data":data}) : helper)))
    + "\" class=\"ps-section\">\n  <div class=\"calendar-title\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</div>\n  <div class=\"calendar-contents\"></div>\n  \n</section>";
},"useData":true});

PS.templates["layout-two-containers"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<section type=\"layout-two-containers\" ps-sectionid=\""
    + escapeExpression(((helper = (helper = helpers.sectionid || (depth0 != null ? depth0.sectionid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"sectionid","hash":{},"data":data}) : helper)))
    + "\" class=\"ps-section\">\n  <div class=\"containers-wrapper\">\n    <div class=\"container1\"></div>\n    <div class=\"container2\"></div>\n  </div>\n</section>";
},"useData":true});

PS.templates["row-event"] = Handlebars.template({"1":function(depth0,helpers,partials,data,depths) {
  var stack1, helper, lambda=this.lambda, escapeExpression=this.escapeExpression, functionType="function", helperMissing=helpers.helperMissing, buffer = "\n                <li class=\"event-static event-item slide__row slide__row-event\">\n                    <div class=\"event-list-item\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.eventDetails : depth0)) != null ? stack1['0'] : stack1)) != null ? stack1.name : stack1), depth0))
    + "</div>\n                    <div class=\"event-list-item\">"
    + escapeExpression(((helper = (helper = helpers.friendlyDateTime || (depth0 != null ? depth0.friendlyDateTime : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"friendlyDateTime","hash":{},"data":data}) : helper)))
    + "</div>\n                    <div class=\"event-list-item\">@"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.eventDetails : depth0)) != null ? stack1['0'] : stack1)) != null ? stack1.meetingPointDescription : stack1), depth0))
    + "</div>\n                    <div class=\"event-list-item\">"
    + escapeExpression(lambda(((stack1 = (depths[1] != null ? depths[1].content : depths[1])) != null ? stack1.eventHashtag : stack1), depth0))
    + "</div>\n                </li>\n\n                <li class=\"event-slide event-item slide__row slide__row-event\">\n                    "
    + escapeExpression(((helper = (helper = helpers.friendlyDateTime || (depth0 != null ? depth0.friendlyDateTime : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"friendlyDateTime","hash":{},"data":data}) : helper)))
    + " ";
  stack1 = lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.eventDetails : depth0)) != null ? stack1['0'] : stack1)) != null ? stack1.name : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n                    <div class=\"location-swipe\"><span>"
    + escapeExpression(((helper = (helper = helpers.friendlyDateTime || (depth0 != null ? depth0.friendlyDateTime : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"friendlyDateTime","hash":{},"data":data}) : helper)))
    + " at "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.eventDetails : depth0)) != null ? stack1['0'] : stack1)) != null ? stack1.meetingPointDescription : stack1), depth0))
    + "</span></div>\n                </li>\n\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,depths) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "    <ul  class=\"slide__row slide__row-order-";
  stack1 = lambda(((stack1 = (depth0 != null ? depth0.content : depth0)) != null ? stack1.rowOrder : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  buffer += " text-"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.content : depth0)) != null ? stack1.alignment : stack1), depth0))
    + "\">\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.content : depth0)) != null ? stack1.events : stack1), {"name":"each","hash":{},"fn":this.program(1, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </ul>";
},"useData":true,"useDepths":true});

PS.templates["row-media"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"slide__row slide__row-media slide__row-order-";
  stack1 = lambda(((stack1 = (depth0 != null ? depth0.content : depth0)) != null ? stack1.rowOrder : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\" style=\"background-image: url("
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.content : depth0)) != null ? stack1.filename : stack1), depth0))
    + ");background-origin: padding-box;background-position: 50% 50%;background-repeat: no-repeat;background-size: auto;\">\n</div>";
},"useData":true});

PS.templates["row-text"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"slide__row slide__row-text slide__row-order-";
  stack1 = lambda(((stack1 = (depth0 != null ? depth0.content : depth0)) != null ? stack1.rowOrder : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  buffer += " text-"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.content : depth0)) != null ? stack1.alignment : stack1), depth0))
    + "\" style=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.content : depth0)) != null ? stack1.textStyle : stack1), depth0))
    + "\">\n    ";
  stack1 = lambda(((stack1 = (depth0 != null ? depth0.content : depth0)) != null ? stack1.text : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n</div>";
},"useData":true});

PS.templates["slide"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return "<section type=\"slide\" ps-sectionid=\""
    + escapeExpression(((helper = (helper = helpers.sectionid || (depth0 != null ? depth0.sectionid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"sectionid","hash":{},"data":data}) : helper)))
    + "\" class=\"ps-section\" style=\"background-image: url("
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.content : depth0)) != null ? stack1.backgroundImage : stack1), depth0))
    + ");background-origin: padding-box;background-position: 50% 50%;background-repeat: no-repeat;background-size: auto;\">\n\n</section>";
},"useData":true});

PS.templates["transition-animate"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"animations\" id=\"anim\">\n    <div id=\"shutters\">\n        <ul>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n            <li><span></span></li>\n        </ul>\n    </div>\n    <div id=\"greenbars\">\n        <ul>\n            <li>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n            </li>\n            <li>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n            </li>\n            <li>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n            </li>\n            <li>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n            </li>\n            <li>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n                <div><span></span></div>\n            </li>\n        </ul>\n    </div>\n    <div id=\"bolt\">\n        <ul>\n            <li></li>\n            <li></li>\n        </ul>\n    </div>\n</div>\n<div class=\"mask\"></div>";
  },"useData":true});

PS.templates["transition-multi-swipe"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"transition multi-swipe\">\n  <div class=\"slide1\">\n    <div></div>\n  </div>\n  <div class=\"slide2\">\n    <div></div>\n  </div>\n</div>";
  },"useData":true});

PS.templates["transition-single-swipe"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"transition single-swipe\">\n  <div class=\"single-slide\">\n    <div></div>\n  </div>\n  <div class=\"slide2\">\n    <div></div>\n  </div>\n</div>";
  },"useData":true});

PS.templates["transition-translate-up"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"transition translate-up\">\n  <div class=\"single-slide\">\n    <div></div>\n  </div>\n</div>";
  },"useData":true});

//# sourceMappingURL=templates.js.map