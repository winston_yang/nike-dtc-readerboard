PS.transitions = {
  $els: {},

  play: function(transition) {
      // check if we need to render the transition html

      if (transition === 'non-animate') {
          return;
      }
      if (!this.$els[transition]) {
        this.$els[transition] = $(PS.templates['transition-' + transition]());
        $('body').append(this.$els[transition]);
      }

      //add extra class to handle translate-up transition
      // $('body').addClass('type-' + transition);


      if (transition === 'animate') {
          $('body').addClass('swype');

          return;

          setTimeout(function() {
              $('body').addClass('in');
          }, 1000);


          setTimeout(function() {
              // $('body').removeClass('type-' + transition);
              $('body').addClass('out');
          }, 2750);

          setTimeout(function() {
              // $('body').removeClass('type-' + transition);
              $('body').removeClass('in');
              $('body').removeClass('out');
          }, 4250);
      } else {
          setTimeout(function() {
              $('body').addClass('play-' + transition);
          }, 0);

          setTimeout(function() {
              // $('body').removeClass('type-' + transition);
              $('body').removeClass('play-' + transition);
          }, 2750);
      }

  }
};