PS.modules.data = PS.utils.extend(PS.modules.data, {
  NanoSource: (function() {
      // var $valueProxy = $('<div/>');

      function processXML(data) {
          var $xml = $(data),
              result = {};

          // console.log('processing data::', $xml);
          // console.log(JSON.parse($valueProxy.html($xml.find('template > #calendar #calendarsources').attr('value')).text()));
          // console.log($xml.find('template > #config').children());

          result.config = processGroup($xml.find('template > #config'), true);
          result.sequence = processGroup($xml.find('template > [id="sequence"]'), true);
          result.sections = processGroup($xml.find('template > [id="sections"]'), 'sectionid');
          result.calendar = processGroup($xml.find('template > [id="calendar"]'), 'sectionid');
          result['day-view'] = processGroup($xml.find('template > [id="day-view"]'), 'sectionid');
          result['fixed-text'] = processGroup($xml.find('template > [id="fixed-text"]'), 'sectionid');
          result['animated-text'] = processGroup($xml.find('template > [id="animated-text"]'), 'sectionid');
          result['layout-two-containers'] = processGroup($xml.find('template > [id="layout-two-containers"]'), 'sectionid');
          result['layout-three-containers'] = processGroup($xml.find('template > [id="layout-three-containers"]'), 'sectionid');
          result['backgroundimages'] = processGroup($xml.find('template > [id="backgroundimages"]'), 'imagebackground');

          // PS.data.config = processGroup($xml.find('template > #config'), true);
          // PS.data.sequence = processGroup($xml.find('template > [id="sequence"]'), true);
          // PS.data.sections = processGroup($xml.find('template > [id="sections"]'), 'sectionid');
          // PS.data.calendar = processGroup($xml.find('template > [id="calendar"]'), 'sectionid');
          // PS.data['fixed-text'] = processGroup($xml.find('template > [id="fixed-text"]'), 'sectionid');

          return result;
      }

      function processGroup($nodes, key) {
          var obj, result,
              asArray = typeof key === 'boolean' ? key : false;

          if (asArray) {
            result = [];
          } else {
            result = {};
          }

          $nodes.each(function(nodeIdx, node) {
              obj = {};

              $(node).children().each(function(i, child) {
                  var $child = $(child),
                      imageList = [],
                      videoList = [],
                      sectionGuid;

                  if (child.getAttribute('id') === "sectionid" && child.getAttribute('value') === "") {
                    sectionGuid = $child.find('values > value').text();

                    obj.sectionid = $child.parents('template').find('> group[guid="' + sectionGuid + '"] > [id="sectionid"]').get(0).getAttribute('value');

                  // set video background
                  } else if (child.getAttribute('id') === "videobackground" && $child.children().length > 0) {
                    // sectionGuid = $child.find('values > value').text();

                    // obj.videobackground = $child.parents('template').find('> group[guid="' + sectionGuid + '"] > [id="filename"]').attr('value');

                    $child.find('> values > value').each(function(i, node) {
                        videoList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.videobackground = videoList;
                  // set landscape video
                  } else if (child.getAttribute('id') === "landscapevideo" && $child.children().length > 0) {
                    // sectionGuid = $child.find('values > value').text();

                    // obj.landscapevideo = $child.parents('template').find('> group[guid="' + sectionGuid + '"] > [id="filename"]').attr('value');

                    $child.find('> values > value').each(function(i, node) {
                        videoList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.landscapevideo = videoList;

                  // set portrait video
                  } else if (child.getAttribute('id') === "portraitvideo" && $child.children().length > 0) {
                    // sectionGuid = $child.find('values > value').text();

                    // obj.portraitvideo = $child.parents('template').find('> group[guid="' + sectionGuid + '"] > [id="filename"]').attr('value');

                    $child.find('> values > value').each(function(i, node) {
                        videoList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.portraitvideo = videoList;

                  // set image background comma separated list
                  } else if (child.getAttribute('id') === "imagebackground" && $child.children().length > 0) {
                    $child = $(child);
                    $child.find('> values > value').each(function(i, node) {
                        imageList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.imagebackground = imageList;

                  // set landscape image background comma separated list
                  } else if (child.getAttribute('id') === "landscapeimage" && $child.children().length > 0) {
                    $child = $(child);
                    $child.find('> values > value').each(function(i, node) {
                        imageList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.landscapeimage = imageList;

                  // set portrait image background comma separated list
                  } else if (child.getAttribute('id') === "portraitimage" && $child.children().length > 0) {
                    $child = $(child);
                    $child.find('> values > value').each(function(i, node) {
                        imageList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.portraitimage = imageList;

                  } else if (child.getAttribute('id') === "text" && node.getAttribute('id') === "animated-text" ) {
                    if (!obj[child.getAttribute('id')]) {
                      obj.text = [];
                    }
                    obj.text.push(child.getAttribute('value'));
                    // obj[child.getAttribute('id')] = child.getAttribute('value');

                  } else if (child.getAttribute('id') === "highlight" && node.getAttribute('id') === "animated-text" ) {
                    if (!obj.highlight) {
                      obj.highlight = [];
                    }
                    obj.highlight.push(child.getAttribute('value'));

                  } else {
                    obj[child.getAttribute('id')] = child.getAttribute('value');
                  }
              });

              if (asArray) {
                result.push(obj);
              } else {
                result[obj[key]] = obj;
              }
          });

          return result;
      }

      function setData(data) {
          for (var prop in data) {
            if (data.hasOwnProperty(prop)) {
              PS.data[prop] = data[prop];
            }
          }
      }

      function checkNewData(rawData, newData) {
          var result = false;

          for (var i in newData) {
            if (newData.hasOwnProperty(i)) {
              for (var j in newData[i]) {
                if (!rawData[i]) {
                  result = true;
                
                } else if (!rawData[i][j]) {
                  result = true;

                } else if (JSON.stringify(newData[i][j]) != JSON.stringify(rawData[i][j])) {
                  result = true;

                  break;
                }
              }
            }

            if (result) {
              break;
            }
          }

          return result;
      }

      return {
        // default data refresh interva 10 minutes
        // overriden in config[0].refreshinterval
        // defaultRefreshInterval: 600000,
        defaultRefreshInterval: 30000,

        // prop to store the data interval
        refreshInterval: null,

        init: function(url, cb) {
            var self = this,
                sourceUrl,
                getUrlParameter = function(sParam) {
                    var sPageURL = window.location.search.substring(1);
                    var sURLVariables = sPageURL.split('&');
                    for (var i = 0; i < sURLVariables.length; i++)
                    {
                        var sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] == sParam)
                        {
                            return sParameterName[1];
                        }
                    }
                };
            //this.isAwesomium = window.navigator.userAgent.toLowerCase().indexOf('awesomium') > -1;
            //replace this.isAwesomium with this.remote to support IE
            this.remote = getUrlParameter("remote");

            if (this.remote === "true") {
              sourceUrl = document.createElement('a');
              sourceUrl.href = url;
              if (sourceUrl.pathname.substr(0,1) === '/' ) {
                  url = location.protocol + '//' + location.host + '/nanodata' + sourceUrl.pathname;
              } else {
                  //for browser like IE
                  url = location.protocol + '//' + location.host + '/nanodata/' + sourceUrl.pathname;
              }

              sourceUrl = null;
            }

            this.url = url;

            this.getData(function(error, data) {
                self.rawData = data;

                if (data) {
                  setData(data);
                }

                cb(error);

                self.setRefreshInterval();
            });
        },

        getData: function(callback) {
            var self = this;

            $.ajax({
              url: this.url + '?_=' + new Date().getTime(),
              dataType: 'xml'
            })
              .done(function(data) {
                  var processedData = processXML(data);

                  if (self.remote === "true" && processedData.config[0].externalasseturl) {
                    processedData.config[0].asseturl = processedData.config[0].externalasseturl;
                  }

                  callback(false, processedData);
              }).
              fail(function() {
                  callback(true);
              });
        },

        setRefreshInterval: function() {
            var self = this;

            if (this.refreshInterval) {
              clearInterval(this.refreshInterval);
            }

            this.refreshInterval = setInterval(function() {
                self.getData(function(error, data) {
                    if (error) {
                      return;
                    }

                    if (checkNewData(self.rawData, data)) {
                      self.rawData = data;

                      data.source = PS.data.source;
                      PS.modules.sequenceManager.refresh(data);
                    }
                });
            }, PS.data.config[0].refreshinterval || this.defaultRefreshInterval);
        }
      };
  })()
});