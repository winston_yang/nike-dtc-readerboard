PS.modules.data = PS.utils.extend(PS.modules.data, {
  NanoSource: (function() {
      var idCount = 0,
          templateData = {};

      function genId() {
          return 'Nike' + idCount++;
      }

      function getConfigFile(data, cb) {
          var $config = $(data).find('template #configFile'),
              url = PS.modules.data.NanoSource.url;

          $.ajax({
            url: url.substring(0, url.lastIndexOf('/')) + '/' + $config.attr('value') + '?_=' + new Date().getTime(),
            dataType: 'json'
          })
            .done(function(data) {
                cb(data);
            }).
            fail(function() {

            });          
      }


      function processFixedText($xml) {
          var $item = $xml.find('template > [name="[Row Text]"]'),
              $target,
              items = {},
              rowTexts = {};

          for (var i = 0, len = $item.length; i < len; ++i) {
              $target = $($item[i]);
              items[$target.attr('guid')] = {
                  text: $target.find('> [id="text"]').attr('value'),
                  fontSize: $target.find('> [id="font-size"]').attr('value'),
                  fontWeight: $target.find('> [id="font-weight"]').attr('value'),
                  alignment: $target.find('> [id="alignment"]').attr('value'),
                  color: $target.find('> [id="color"]').attr('value'),
                  name: $target.find('> [id="name"]').attr('value'),
                  type: 'row-text',
                  rowOrder: $target.find('> [id="row-order"]').attr('value')
              };
          }

//          $item = $xml.find('template > [name="[Row Text]"]');
//
//          for (var i = 0, len = $item.length; i < len; ++i) {
//              $target = $($item[i]);
//              $relatedText = items[$target.find('#text value').text()];
//              rowTexts[$target.attr('guid')] = {
//                  text: $relatedText.text,
//                  fontSize: $relatedText.fontSize,
//                  fontWeight: $relatedText.fontWeight,
//                  color: $relatedText.color,
//                  alignment: $relatedText.alignment,
//                  type: 'row-text',
//                  rowOrder: $target.find('> [id="row-order"]').attr('value'),
//                  name: $target.find('> [id="name"]').attr('value')
//              };
//          }

          return items;
      }

      function processEvents($xml) {
          var $item = $xml.find('template > [name="[Row Events]"]'),
              $target,
              items = {};

          for (var i = 0, len = $item.length; i < len; ++i) {
              $target = $($item[i]);
              items[$target.attr('guid')] = {
                  rowOrder: $target.find('> [id="row-order"]').attr('value'),
                  eventType: $target.find('> [id="event-type"]').attr('value'),
                  multiLine: $target.find('> [id="Multi-line"]').attr('value'),
                  eventTemplate: $target.find('> [id="event-template"]').attr('value'),
                  eventHashtag: $target.find('> [id="event-hashtag"]').attr('value'),
                  alignment: $target.find('> [id="alignment"]').attr('value'),
                  type: 'row-event',
                  name: $target.find('> [id="name"]').attr('value')
              };
          }

          return items;
      }

      function processMedia($xml) {
          var $item = $xml.find('template > [name="[Row Media]"]'),
              $target,
              items = {};

          for (var i = 0, len = $item.length; i < len; ++i) {
              $target = $($item[i]);
              relatedMedia = templateData.backgroundimages[$target.find('#background value').text()];
              items[$target.attr('guid')] = {
                  rowOrder: $target.find('> [id="row-order"]').attr('value'),
                  background:$target.find('> [id="row-order"]').attr('value'),
                  type: 'row-media',
                  filename: relatedMedia.filename,
                  name: $target.find('> [id="name"]').attr('value')
              };
          }

          return items;
      }

      function processBackgroundImages($xml) {
          var $media = $xml.find('template > [name="[Media]"]'),
              images = {};

          for (var i = 0, len = $media.length; i < len; ++i) {
            images[$($media[i]).attr('guid')] = {
              eventtype: $($media[i]).find('> [id="eventtype"]').attr('value'),
              filename: $($media[i]).find('> [id="source"]').attr('value'),
              orientation: $($media[i]).find('> [id="orientation"]').attr('value'),
              name: $($media[i]).find('> [id="name"]').attr('value')
            };
          }

          return images;
      }

      function processSection(type, $node, defaults, sectionId) {
          var result = $.extend({}, defaults);

          $node.children().each(function(i, child) {
              var $child = $(child),
                  backgrounds = [];
                  rowTexts = [],
                  rowEvents = [],
                  rowMedias = [];

              if (child.getAttribute('id') == 'background-image' && $child.children().length > 0) {
                // TODO check if background is an image or video
                $child.find('> values > value').each(function(i, node) {
                    backgrounds.push(templateData.backgroundimages[$(node).text()].filename);
                });

                result.landscapeimage = backgrounds;
              } else if (child.getAttribute('id') === "row-text") {

                  // Check for row of text
                  $child.find('> values > value').each(function(i, node) {
                      rowTexts.push(templateData.rowTexts[$(node).text()]);
                  });

                  result.rowTexts = rowTexts;

              } else if (child.getAttribute('id') === "row-events") {
                  // Check for row of events
                  $child.find('> values > value').each(function(i, node) {
                      rowEvents.push(templateData.rowEvents[$(node).text()]);
                  });

                  result.rowEvents = rowEvents;
              } else if (child.getAttribute('id') === "row-media") {
                  // Check for row of events
                  $child.find('> values > value').each(function(i, node) {
                      rowMedias.push(templateData.rowMedias[$(node).text()]);
                  });

                  result.rowMedias = rowMedias;
              } else if (child.getAttribute('value')) {
                result[child.getAttribute('id')] = child.getAttribute('value');
              }
          });

          result.type = type;
          result.sectionid = sectionId;

          if (!templateData[type]) {
            templateData[type] = {};
          }

          templateData[type][sectionId] = result;

          return result;
      }

      function processLayout(layout, config, layoutId, sectionId) {
          // TODO make this more dynamic, right now it's hardcoded for 2 containers
          var layoutContainers;

          layout = layout || config.defaults.layout;
          layoutContainers = $.extend({}, config.layouts[layout]);

          layoutContainers.container1 = sectionId;
          layoutContainers.control = sectionId;

          if (!templateData['layout-' + layout]) {
            templateData['layout-' + layout] = {};
          }
          templateData['layout-' + layout][layoutId] = layoutContainers;

          return {
            type: 'layout-two-containers'
          }
      }

      function processXML(data, cb) {
          var $xml = $(data);

          getConfigFile(data, function(jsonConfig) {
              templateData.config = jsonConfig.fields.config;
              templateData.config.sereisId = $xml.find('template #appConfig #seriesID').attr('value');
              templateData.config.eventAnimation = $xml.find('template #appConfig #slide-transition').attr('value');
              templateData.backgroundimages = processBackgroundImages($xml);
              templateData.rowTexts = processFixedText($xml);
              templateData.rowEvents = processEvents($xml);
              templateData.rowMedias = processMedia($xml);

              templateData.sections = {};
              //templateData.sections['footer'] = processSection('fixed-text', $xml.find('template > [name="[footer]"]'), jsonConfig.fields['fixed-text'], 'footer');


              templateData.sequence = [];

              // create sections
              $xml.find('template > group').each(function(i, node) {
                  var $node = $(node),
                      sectionId, sectionType,
                      layout, layoutId;

                  if ((node.getAttribute('name') && node.getAttribute('name')[0] === '[') || node.getAttribute('id') === 'appConfig') {
                    return;
                  }

                  if ($node.find('> [id="layout"]').length === 0) {
                    layout = jsonConfig.defaults.layout;
                  }

                  sectionId = genId();
                  layoutId = genId();

                  sectionType = node.getAttribute('id')
                  templateData.sections[sectionId] = processSection(sectionType, $node, jsonConfig.fields[sectionType], sectionId);

                  templateData.sections[layoutId] = processLayout(templateData.sections[sectionId].layout, jsonConfig, layoutId, sectionId);
                  templateData.sections[layoutId].sectionid = layoutId;


                  templateData.sequence.push({
                    sectionid: layoutId,
                    transition: $xml.find('template #appConfig #slide-transition').attr('value')
                  });
              });




              cb(templateData);
          });

          return;
          // console.log('processing data::', $xml);
          // console.log(JSON.parse($valueProxy.html($xml.find('template > #calendar #calendarsources').attr('value')).text()));
          // console.log($xml.find('template > #config').children());


          result.config = processGroup($xml.find('template > #config'), true);
          result.sequence = processGroup($xml.find('template > [id="sequence"]'), true);
          result.sections = processGroup($xml.find('template > [id="sections"]'), 'sectionid');
          result.calendar = processGroup($xml.find('template > [id="calendar"]'), 'sectionid');
          result['day-view'] = processGroup($xml.find('template > [id="day-view"]'), 'sectionid');
          result['fixed-text'] = processGroup($xml.find('template > [id="fixed-text"]'), 'sectionid');
          result['animated-text'] = processGroup($xml.find('template > [id="animated-text"]'), 'sectionid');
          result['layout-two-containers'] = processGroup($xml.find('template > [id="layout-two-containers"]'), 'sectionid');
          result['layout-three-containers'] = processGroup($xml.find('template > [id="layout-three-containers"]'), 'sectionid');
          result['backgroundimages'] = processGroup($xml.find('template > [id="backgroundimages"]'), 'imagebackground');

          // PS.data.config = processGroup($xml.find('template > #config'), true);
          // PS.data.sequence = processGroup($xml.find('template > [id="sequence"]'), true);
          // PS.data.sections = processGroup($xml.find('template > [id="sections"]'), 'sectionid');
          // PS.data.calendar = processGroup($xml.find('template > [id="calendar"]'), 'sectionid');
          // PS.data['fixed-text'] = processGroup($xml.find('template > [id="fixed-text"]'), 'sectionid');

          return result;
      }

      function processGroup($nodes, key) {
          var obj, result,
              asArray = typeof key === 'boolean' ? key : false;

          if (asArray) {
            result = [];
          } else {
            result = {};
          }

          $nodes.each(function(nodeIdx, node) {
              obj = {};

              $(node).children().each(function(i, child) {
                  var $child = $(child),
                      imageList = [],
                      videoList = [],
                      sectionGuid;

                  if (child.getAttribute('id') === "sectionid" && child.getAttribute('value') === "") {
                    sectionGuid = $child.find('values > value').text();

                    obj.sectionid = $child.parents('template').find('> group[guid="' + sectionGuid + '"] > [id="sectionid"]').get(0).getAttribute('value');

                  // set video background
                  } else if (child.getAttribute('id') === "videobackground" && $child.children().length > 0) {
                    // sectionGuid = $child.find('values > value').text();

                    // obj.videobackground = $child.parents('template').find('> group[guid="' + sectionGuid + '"] > [id="filename"]').attr('value');

                    $child.find('> values > value').each(function(i, node) {
                        videoList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.videobackground = videoList;
                  // set landscape video
                  } else if (child.getAttribute('id') === "landscapevideo" && $child.children().length > 0) {
                    // sectionGuid = $child.find('values > value').text();

                    // obj.landscapevideo = $child.parents('template').find('> group[guid="' + sectionGuid + '"] > [id="filename"]').attr('value');

                    $child.find('> values > value').each(function(i, node) {
                        videoList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.landscapevideo = videoList;

                  // set portrait video
                  } else if (child.getAttribute('id') === "portraitvideo" && $child.children().length > 0) {
                    // sectionGuid = $child.find('values > value').text();

                    // obj.portraitvideo = $child.parents('template').find('> group[guid="' + sectionGuid + '"] > [id="filename"]').attr('value');

                    $child.find('> values > value').each(function(i, node) {
                        videoList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.portraitvideo = videoList;

                  // set image background comma separated list
                  } else if (child.getAttribute('id') === "imagebackground" && $child.children().length > 0) {
                    $child = $(child);
                    $child.find('> values > value').each(function(i, node) {
                        imageList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.imagebackground = imageList;

                  // set landscape image background comma separated list
                  } else if (child.getAttribute('id') === "landscapeimage" && $child.children().length > 0) {
                    $child = $(child);
                    $child.find('> values > value').each(function(i, node) {
                        imageList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.landscapeimage = imageList;

                  // set portrait image background comma separated list
                  } else if (child.getAttribute('id') === "portraitimage" && $child.children().length > 0) {
                    $child = $(child);
                    $child.find('> values > value').each(function(i, node) {
                        imageList.push($child.parents('template').find('> group[guid="' + $(node).text() + '"] > [id="filename"]').attr('value'));
                    });
                    obj.portraitimage = imageList;

                  } else if (child.getAttribute('id') === "text" && node.getAttribute('id') === "animated-text" ) {
                    if (!obj[child.getAttribute('id')]) {
                      obj.text = [];
                    }
                    obj.text.push(child.getAttribute('value'));
                    // obj[child.getAttribute('id')] = child.getAttribute('value');

                  } else if (child.getAttribute('id') === "highlight" && node.getAttribute('id') === "animated-text" ) {
                    if (!obj.highlight) {
                      obj.highlight = [];
                    }
                    obj.highlight.push(child.getAttribute('value'));

                  } else {
                    obj[child.getAttribute('id')] = child.getAttribute('value');
                  }
              });

              if (asArray) {
                result.push(obj);
              } else {
                result[obj[key]] = obj;
              }
          });

          return result;
      }

      function setData(data) {
          for (var prop in data) {
            if (data.hasOwnProperty(prop)) {
              PS.data[prop] = data[prop];
            }
          }
      }

      function checkNewData(rawData, newData) {
          var result = false;

          for (var i in newData) {
            if (newData.hasOwnProperty(i)) {
              for (var j in newData[i]) {
                if (!rawData[i]) {
                  result = true;
                
                } else if (!rawData[i][j]) {
                  result = true;

                } else if (JSON.stringify(newData[i][j]) != JSON.stringify(rawData[i][j])) {
                  result = true;

                  break;
                }
              }
            }

            if (result) {
              break;
            }
          }

          return result;
      }

      return {
        // default data refresh interva 10 minutes
        // overriden in config.refreshinterval
        // defaultRefreshInterval: 600000,
        defaultRefreshInterval: 30000,

        // prop to store the data interval
        refreshInterval: null,

        init: function(url, cb) {
            var self = this,
                sourceUrl,
                getUrlParameter = function(sParam) {
                    var sPageURL = window.location.search.substring(1);
                    var sURLVariables = sPageURL.split('&');
                    for (var i = 0; i < sURLVariables.length; i++)
                    {
                        var sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] == sParam)
                        {
                            return sParameterName[1];
                        }
                    }
                };
            //this.isAwesomium = window.navigator.userAgent.toLowerCase().indexOf('awesomium') > -1;
            //replace this.isAwesomium with this.remote to support IE
            this.remote = getUrlParameter("remote");

            if (this.remote === "true") {
              sourceUrl = document.createElement('a');
              sourceUrl.href = url;
              if (sourceUrl.pathname.substr(0,1) === '/' ) {
                  url = location.protocol + '//' + location.host + '/nanodata' + sourceUrl.pathname;
              } else {
                  //for browser like IE
                  url = location.protocol + '//' + location.host + '/nanodata/' + sourceUrl.pathname;
              }

              sourceUrl = null;
            }

            this.url = url;

            this.getData(function(error, data) {
                self.rawData = data;

                if (data) {
                  setData(data);
                }

                cb(error);

                // self.setRefreshInterval();
            });
        },

        getData: function(callback) {
            var self = this;

            $.ajax({
              url: this.url + '?_=' + new Date().getTime(),
              dataType: 'xml'
            })
              .done(function(data) {
                  processXML(data, function(processedXmlData) {
                      if (self.remote === "true" && processedXmlData.config.externalasseturl) {
                        processedXmlData.config.asseturl = processedXmlData.config.externalasseturl;
                      }

                      callback(false, processedXmlData);
                  });
                  // var processedData = processXML(data);

                  
              }).
              fail(function() {
                  callback(true);
              });
        },

        setRefreshInterval: function() {
            var self = this;

            if (this.refreshInterval) {
              clearInterval(this.refreshInterval);
            }

            this.refreshInterval = setInterval(function() {
                self.getData(function(error, data) {
                    if (error) {
                      return;
                    }

                    if (checkNewData(self.rawData, data)) {
                      self.rawData = data;

                      data.source = PS.data.source;
                      PS.modules.sequenceManager.refresh(data);
                    }
                });
            }, PS.data.config.refreshinterval || this.defaultRefreshInterval);
        }
      };
  })()
});