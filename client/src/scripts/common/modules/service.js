PS.modules.service = {
  requestCalendar: function(param, cb) {
      var _url;


      if (typeof param === "object") {
          //events/series/{series_id}?from={from}&to={to}&byLocation={byLocation}
          _url =  param.url + param.calendarsources + '?from=' + param.from + '&to=' + param.to + '&callback=?';
          console.log('_url=' + _url);

          $.getJSON(_url, function(data, status, xhr) {
              console.log('data=' + data);
            cb(data);

        }).fail(function() {
            cb([]);
        });

      }
  }
};
///events/series/{series_id}?from={from}&to={to}&byLocation={byLocation}
//url: PS.data.config.eventcalendarurl,
//    calendarsources: PS.data.config..sereisId,
//    from: moment().startOf('isoweek'),
//    to: moment().startOf('isoweek')