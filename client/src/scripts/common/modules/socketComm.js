PS.modules.socketComm = {
    reconnectAttemptsCount: 0,
    reconnectTimeout: null,
    reconnectPingDelay: 20000,

    init: function() {
        var self = this;

        this.socket = io.connect('http://' + window.location.host);

        this.socket.on('connect', function() {
            self.sendClient();
        });

        this.socket.on('refreshdata', function(data) {
            PS.modules.sequenceManager.refresh(data);
        });

        this.socket.on('reload', function(data) {
            PS.modules.sequenceManager.reload(data.time);
        });

        // this.socket.on('disconnect', function() {
        //   console.log('disconnect', self.socket, io);
        // });

        this.socket.on('reconnecting', function(attemptTime, attemptCount) {
            self.reconnectAttemptsCount = attemptCount;

            if (attemptCount === 10) {
              self.reconnectTimeout = setTimeout(function() {
                  self.reconnect();
              }, attemptTime + 100);
            }
        });
    },

    sendClient: function() {
        this.socket.emit('clientready', {
          experienceId: PS.experienceId,
          isNano: PS.data.source && PS.data.source.key === "Nano"
        });
    },

    reconnect: function() {
        var self = this;

        function ping() {
            PS.modules.service.ping(function(success) {
                if (!success) {
                  setTimeout(ping, self.reconnectPingDelay);

                } else {
                  self.socket.socket.connect();
                }
            });
        }
        
        setTimeout(ping, this.reconnectPingDelay);
    }
};