PS.modules.sequenceManager = {
  sequence: [],
  currentSectionIdx: 0,
  previousSectionIdx: 0,
  refreshExperience: false,
  loopCount: 1,

  init: function(cb) {
      var self = this;
      console.log('sequence manager init');

      // if data source is set then fetch data before setting sequence
      if (PS.data.source) {
        PS.modules.data[PS.data.source.key + 'Source'].init(PS.data.source.value, function(error) {
            if (!error) {
              self.setSequenceData();

              if (cb) {
                cb();
              }
            }
        });

      } else {
        this.setSequenceData();

        if (cb) {
          cb();
        }
      }
  },

  // check and set all sequence section data before launching
  setSequenceData: function(isRefresh) {
      var self = this,
          sections = PS.data.sections,
          dataRequestsCount = 0,
          dataRequestsComplete = 0,
          sectionData;

      function checkDataRequests() {
          dataRequestsComplete++;

          if (dataRequestsCount === dataRequestsComplete) {
            self.renderSections();
            self.startSequence();
          }
      }

      // $.get('nanoCmd://system/writeToLogFile?load=startsequence').done(function() {alert("ok")}).fail(function() {alert('f');});

      this.sequence = PS.data.sequence;

      for (var sectionId in sections) {
        if (isRefresh) {
          PS.sections.destroy(sectionId);
        }

        sectionData = PS.sections.getSectionData(sectionId);

        if (sectionData.jsController.setData) {
          dataRequestsCount++;
          sectionData.jsController.setData(sectionId, checkDataRequests);
        }
      }

      dataRequestsCount++;

      PS.sections.calendar.setData(checkDataRequests);

      if (dataRequestsCount === 0) {
        dataRequestsCount = 1;
        checkDataRequests();
      }
  },

  renderSections: function() {
      var sequence = this.sequence;

      for (var i = 0, len = sequence.length; i < len; ++i) {
        PS.sections.create(sequence[i].sectionid);
      }
  },

  startSequence: function() {
      var self = this;

      PS.sections.show(this.sequence[0]);

      PS.main.$sectionsContainer.on('endanimation', function(ev, id) {
          self.playTransition(id);
      });
  },

  resetSequence: function() {
      PS.main.$sectionsContainer.off('endanimation');

      PS.data = this.refreshData;
      this.refreshData = null;
      this.refreshExperience = false;

      this.previousSectionIdx = 0;
      this.currentSectionIdx = 0;

      this.setSequenceData(true);
  },

  showNext: function() {
      this.previousSectionIdx = this.currentSectionIdx;

      this.getNewCurrentIndex();

      // refresh browser
      if (this.refreshExperience && this.currentSectionIdx === 0) {
        // if (!this.refreshData) {
          location.reload();
        
        // } else {
        //   this.resetSequence();
        // }

      } else {
        PS.sections.hide(this.sequence[this.previousSectionIdx].sectionid);

        PS.sections.show(this.sequence[this.currentSectionIdx]);
      }

      if (this.currentSectionIdx === 0) {
        this.loopCount++;
      }
  },

  getNewCurrentIndex: function() {
      var sectionData;

      this.currentSectionIdx = this.currentSectionIdx === this.sequence.length - 1 ? 0 : this.currentSectionIdx + 1;

      if (this.sequence[this.currentSectionIdx].loopcycle &&
          (this.loopCount % this.sequence[this.currentSectionIdx].loopcycle) !== 0) {

        this.currentSectionIdx = this.currentSectionIdx + 1 === this.sequence.length ? 0 : this.currentSectionIdx + 1;
      }
      
      sectionData = PS.sections.getSectionData(this.sequence[this.currentSectionIdx].sectionid);

      if (sectionData.jsController.canPlay && !sectionData.jsController.canPlay(sectionData.sectionid)) {
        this.getNewCurrentIndex();
      }
  },


  playTransition: function(id) {
      var transition = this.sequence[this.currentSectionIdx].transition || "multi-swipe",
          self = this;

      PS.transitions.play(transition);

      setTimeout(function() {
          self.showNext();
      }, 800);
  },

  // mark experience for refresh
  refresh: function(data) {
      this.refreshExperience = true;

      if (data) {
        this.refreshData = data;
      }
  },

  reload: function(reloadTime) {
      var self = this;

      setTimeout(function() {
          PS.modules.service.ping(function(isAlive) {
              if (isAlive) {
                self.refresh();

              } else {
                self.reload(reloadTime);
              }
          })
      }, reloadTime);
  }
};