PS.utils = {
  extend: function(src, obj, moduleExtension) {
      if (moduleExtension) {
        obj._super = src;
      }

      for (var prop in src) {
        if (src.hasOwnProperty(prop) && !obj.hasOwnProperty(prop)) {
          obj[prop] = src[prop];
        }
      }
      return obj;
      // return Object.create(obj);
  },

  toCamelCase: function(str) {
      return str.replace(/-([a-z])/g, function (g) {
          return g[1].toUpperCase();
      });
  },

  fixNikePlus: function(str) {
      if (!str) {
        return '';
      } else {
        return str.replace(/nike\+/ig, 'nike<span class="plus-symbol">+</span>');
      }
  },

  replaceImages: function(str) {
      var re = /(\[\[[\w\s-_]+\]\])/gi,
          matches,
          imgSrc,
          img;

      matches = str.match(re);

      if (!matches || !matches.length) {
        return str;
      }

      function getImage(imgName) {
          var imgMatch;

          for (var imgHash in PS.data.backgroundimages) {
             if (PS.data.backgroundimages[imgHash].name === imgName) {
              return PS.data.backgroundimages[imgHash].filename;
             }
          }
      }

      for (var i = 0, len = matches.length; i < len; ++i) {

        // imgSrc = PS.data.config.asseturl + PS.data.backgroundimages[ matches[i].substring(2, matches[i].length - 2) ].filename;
        imgSrc = PS.data.config.asseturl + getImage(matches[i].substring(2, matches[i].length - 2));
        img = '<img src="' + imgSrc + '" />'
        str = str.split(matches[i]).join(img);
      }

      return str;
  }
};