/**
 * sections helper methods
 */
PS.sections = PS.utils.extend(PS.sections, {
  dataCache: {},

  getSectionData: function(id) {
      var sectionConfig = PS.data.sections[id],
          sectionData,
          template,
          data;

      if (this.dataCache[id]) {
        return this.dataCache[id];
      }

      sectionConfig = PS.data.sections[id];

      if (!sectionConfig) {
        return false;
      }

      sectionData = PS.data[sectionConfig.type][id];
      template = PS.templates[sectionConfig.type];

      data = {};

      PS.utils.extend(sectionConfig, data, false);
      PS.utils.extend(sectionData, data, false);
      data.template = template;
      data.jsController = PS.sections[PS.utils.toCamelCase(sectionConfig.type)];

      this.dataCache[id] = data;

      return data;
  },

  // create section El and assign to data object cache
  create: function(id) {
      var data = this.getSectionData(id);

      if (this.getEl(id)) {
        this.destroyEl(id);
      }

      PS.main.$sectionsContainer.append( this.render(id) );
  },

  // generate the section el markup
  render: function(id) {
      var data = this.getSectionData(id);

      if (!data.$el) {
        data.$el = data.jsController.render(id);
        this.setSectionBackground(id);

        return data.$el;
      
      } else {
        return data.$el.clone();
      }
  },

  setSectionBackground: function(id) {
      var data = this.getSectionData(id),
          isPortrait = $(window).height() > $(window).width();

      if (isPortrait) {
        if (data.portraitimage) {
          data.imagebackground = data.portraitimage;
        }
        if (data.portraitvideo) {
          data.videobackground = data.portraitvideo;
        }
      
      } else {
        if (data.landscapeimage) {
          data.imagebackground = data.landscapeimage;
        }
        if (data.landscapevideo) {
          data.videobackground = data.landscapevideo;
        }
      }
      // if there's an image background
      // if (data.imagebackground) {
      //   if (PS.data.config.asseturl) {
      //     data.imagebackground = PS.data.config.asseturl + data.imagebackground;

      //   } else if (data.imagebackground.indexOf('http') === -1) {
      //     data.imagebackground = '../assets/img/' + data.imagebackground;
      //   }

      //   data.$el.css('background-image', 'url(' + data.imagebackground + ')');
      //   data.$el.addClass('image-background');
      // }

      // if there's a video background
      if (data.videobackground) {
        // if (PS.data.config.asseturl) {
        //   data.videobackground = PS.data.config.asseturl + data.videobackground;
        
        // } else if (data.videobackground.indexOf('http') === -1) {
        //   data.videobackground = '../assets/videos/' + data.videobackground;
        // }

        this.setVideoBackground(data);
      }
  },

  setVideoBackground: function(sectionData) {
      var videoUrl,
          $video;

      sectionData.video = {
        els: []
        // videoIndex: 0
      };

      for (var i = 0, len = sectionData.videobackground.length; i < len; ++i) {
        if (PS.data.config.asseturl) {
          videoUrl = PS.data.config.asseturl + sectionData.videobackground[i];

        } else if (sectionData.videobackground[i].indexOf('http') === -1) {
          videoUrl = '../assets/videos/' + sectionData.videobackground[i];
        
        } else {
          videoUrl = sectionData.videobackground[i];
        }

        $video = $('<video muted preload="auto" class="video-background"><source src="' + videoUrl + '" /></video>');
        
        sectionData.$el.prepend($video);
        sectionData.video.els.push({$video: $video});
      }


      // if (videoIndex === sectionData.videobackground.length) {
      //   videoIndex = 0;
      // }

      // sectionData.$el.prepend($video);
      // sectionData.video = {
      //   $el: $video
      // };
  },

  setImageBackground: function(sectionData, $el) {
      var imageIndex = typeof sectionData.backgroundIndex !== "undefined" ? sectionData.backgroundIndex + 1 : 0,
          imgageUrl = '';

      $el = $el || sectionData.$el;

      if (!sectionData.imagebackground || !sectionData.imagebackground.length) {
        return;
      }

      if (imageIndex === sectionData.imagebackground.length) {
        imageIndex = 0;
      }

      if (PS.data.config.asseturl) {
        imgageUrl = PS.data.config.asseturl + sectionData.imagebackground[imageIndex];
      
      } else if (sectionData.imagebackground[imageIndex].indexOf('http') === -1) {
        imgageUrl = '../assets/img/' + sectionData[imageIndex];
      }

      $el.css('background-image', 'url(' + imgageUrl + ')');
      $el.addClass('image-background');

      sectionData.backgroundIndex = imageIndex;
  },

  removeVideoBackground: function() {

  },

  runVideoBackground: function(data, cb) {
      var videoIndex = typeof data.video.videoIndex !== "undefined" ? data.video.videoIndex + 1 : 0,
          $videoEl;// = data.video.$el;

      if (videoIndex === data.video.els.length) {
        videoIndex = 0;
      }
      
      data.video.videoIndex = videoIndex;

      if (data.video.els[videoIndex].duration) {
        $videoEl = data.video.els[videoIndex].$video;
        $videoEl.get(0).play();

        $videoEl.addClass('play');

        cb();
      
      } else {
        $videoEl = data.video.els[videoIndex].$video;

        $videoEl.on('canplaythrough', function() {
            var el = $videoEl.get(0);

            $videoEl.off('canplaythrough');

            data.video.els[videoIndex].duration = el.duration * 1000 - 1000;
            
            el.play();

            cb();
        });
        
        $videoEl.addClass('play');
        $videoEl.get(0).load();
        $videoEl.get(0).play();
      }
  },
  stopVideoBackground: function(data) {
      var videoEl = data.video.els[data.video.videoIndex].$video.get(0);

      $(videoEl).removeClass('play');
      try {
        videoEl.currentTime = 0;
        videoEl.pause();
      } catch(e) {
      }
  },

  show: function(section) {
      var data = this.getSectionData(section.sectionid);

      data.$el.removeClass('hide');
      data.$el.addClass('show');

      if (data.video) {
        this.runVideoBackground(data, function() {
            data.jsController.animate(data.sectionid, data.video.els[data.video.videoIndex].duration);
        });

      } else {
        this.setImageBackground(data);
        data.jsController.animate(data.sectionid, section.duration);
      }
  },

  hide: function(id) {
      var sectionData = this.getSectionData(id), targetSection =this.getEl(id);

      targetSection.addClass('hide');
      targetSection.removeClass('show');


      setTimeout(function() {
          targetSection.removeClass('hide');
      }, 1000);

      if (sectionData.video) {
        this.stopVideoBackground(sectionData);
      }
  },

  getEl: function(id) {
      var data = this.getSectionData(id);
      
      if (!data) {
        return false;
      }

      return data.$el;
  },

  destroy: function(id) {
      this.destroyEl(id);

      this.dataCache[id] = null;
  },

  destroyEl: function(id) {
      var data = this.getSectionData(id),
          videoEl;

      if (!data) {
        return false;
      }

      if (data.video) {
        // var videoEl = data.video.$el.get(0);

        for (var i = 0, len = data.video.els.length; i < len; ++i) {
          videoEl = data.video.els[i].$video.get(0);

          videoEl.currentTime = 0;
          videoEl.pause();
          videoEl.src = "";
          data.video.els[i].$video.children('source').attr('src', '');
          data.video.els[i].$video.remove().length = 0;
        }
      }

      if (data.$el) {
        data.$el.remove();
        data.$el = null;
      }
  }
}, false);