PS.sections.calendar = {
  defaultItemScrollTime: 2000,
  defaultCalendarRefreshTime: 600000, // 10 minutes

  defaultMaxCalendarItems: 3,

  animationTime: 1000,

  updateIntervals: {},

  hourOffset: moment().zone() / 60,

  events:{},

  formatFriendlyDateTime: function(events) {
      var shortDays = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
          eventDate,
          day,
          time,
          dateMoment;

      if (!events) {
        return;
      }
      
      for (var i = 0, len = events.length; i < len; ++i) {
        eventDate = new Date(events[i].startDate);
        eventDate.setHours(eventDate.getHours() + this.hourOffset);
        day = shortDays[eventDate.getDay()] + ' ' + (eventDate.getMonth() + 1) + '.' + eventDate.getDate();
        dateMoment = new moment(eventDate);
        time = dateMoment.format(dateMoment.minute() === 0 ? 'hA' : 'h:mmA');

        events[i].friendlyDateTime = day + ' - ' + time;
      }
  },

  groupEventsByType: function(type, data) {
      var events = data.events,
          groupedEvents = [],
          eventDate,
          eventDay,
          groupedEventIndex = -1

      if (!events) {
        return;
      }

      for (var i = 0, len = events.length; i < len; ++i) {
        eventDate = new Date(events[i].startDate);

        if (eventDay !== eventDate.getDate()) {
            eventDay = eventDate.getDate();
            groupedEventIndex = -1;
          // finish grouping
        }

        if (events[i].type.toLowerCase() === type) {
          if (groupedEventIndex === -1) {
            groupedEventIndex = groupedEvents.length;
            groupedEvents.push($.extend({}, events[i]));
            groupedEvents[groupedEventIndex].isGrouped = true;
            groupedEvents[groupedEventIndex].events = [events[i]];
          
          } else {
            // setting to startDate instead of endDate because most of the times that's not set
            groupedEvents[groupedEventIndex].endDate = events[i].startDate;
            groupedEvents[groupedEventIndex].events.push(events[i]);
          }

        } else {
          groupedEvents.push(events[i]);
        }
      }

      data.events = groupedEvents;
  },

  setData: function(cb, refresh) {
      var self = this,
          updateTime;

      function requestCallback(data) {
          self.formatFriendlyDateTime(data.events);

//          if (sectionData.groupbytype) {
//            self.groupEventsByType(sectionData.groupbytype.toLowerCase(), data);
//          }

//          if (refresh) {
//            sectionData.refreshEvents = data.events;
//
//          } else {
//            sectionData.events = data.events;
//          }

          self.events = data.events;

          self.formatFriendlyDateTime(self.events);

          if (cb) {
            cb();
          }
      }

      if (PS.data.source) {
        PS.modules.service.requestCalendar({
          url: PS.data.config.eventcalendarurl,
          calendarsources: PS.data.config.sereisId,
          from: moment().startOf('isoweek').format('YYYY-MM-DD'),
          to: moment().endOf('isoweek').format('YYYY-MM-DD')
        
        }, requestCallback);

      } else {
        PS.modules.service.requestCalendar(sectionData.sectionid, requestCallback);
      }

//      if (!refresh && this.updateIntervals[id]) {
//        clearInterval(this.updateIntervals[id]);
//        this.updateIntervals[id] = null;
//      }
//
//      if (!this.updateIntervals[id]) {
//        updateTime = sectionData.calendarrefreshminutes ? sectionData.calendarrefreshminutes * 60000 : this.defaultCalendarRefreshTime;
//        this.updateIntervals[id] = setInterval(function() {
//            self.setData(id, null, true);
//        }, updateTime);
//      }
  },

//  calcCalendarTop: function(id) {
//
//  },
//
//  generateItem: function(data) {
//      data.title = data.title.replace(/nike\+/gi, 'nike<span class="plus">+</span>');
//
//      return $('<li><span class="event-title">' + data.title + '</span><br /><span>' + data.friendlyDateTime + '</span><br /><span>at ' + data.location + '</li>');
//  },

//  render: function(id) {
//      var sectionData = PS.sections.getSectionData(id),
//          $el;
//
//      if (!sectionData) {
//        return false;
//      }
//
//      $el = $(sectionData.template({
//        sectionid: sectionData.sectionid
//      }));
//
//      sectionData.$calendarHidden = $el.find('.calendar-hidden');
//      sectionData.$calendarList = $el.find('.calendar-display');
//
//      return $el;
//  },
//
//  renderCalendarItems: function(id) {
//      var sectionData = PS.sections.getSectionData(id),
//          calendarData;
//
//      if (sectionData.refreshEvents) {
//        sectionData.events = sectionData.refreshEvents;
//        sectionData.refreshEvents = null;
//      }
//
//      calendarData = sectionData.events;
//
//      sectionData.$calendarHidden.empty();
//      sectionData.$calendarList.empty();
//
//      var item, calendarTop, addedItem,
//          curItemIndex = 0,
//          maxCalendarItems = sectionData.maxcalendaritems || this.defaultMaxCalendarItems;
//
//      sectionData.calendarItemIndex = curItemIndex;
//
//      for (var i = 0, len = calendarData.length; i < len; i++) {
//        item = calendarData[i];
//        item.el = this.generateItem(item);
//        sectionData.$calendarHidden.append(item.el);
//        item.height = item.el.outerHeight();
//      }
//
//      sectionData.itemDisplayQty = calendarData.length > maxCalendarItems ? maxCalendarItems : calendarData.length;
//
//      calendarTop = this.calcCalendarTop(id);
//
//      for (var n = 0; n < sectionData.itemDisplayQty; n++) {
//        addedItem = calendarData[curItemIndex].el.clone();
//        addedItem.css('top', calendarTop);
//        calendarTop += calendarData[curItemIndex].height;
//        sectionData.$calendarList.append(addedItem);
//
//        curItemIndex++;
//
//        if (curItemIndex === calendarData.length) {
//          curItemIndex = 0;
//        }
//      }
//  },
//
//  animate: function(id, duration, preventTrigger) {
//      var sectionData = PS.sections.getSectionData(id),
//          $el = sectionData.$el,
//          calendarData = sectionData.events,
//          maxCalendarItems = sectionData.maxcalendaritems || this.defaultMaxCalendarItems,
//          itemScrollTime = sectionData.itemscrolltime || this.defaultItemScrollTime,
//          self = this;
//
//      this.renderCalendarItems(id);
//
//      function startCalendar() {
//          function animateCalendar() {
//              var $listItems = sectionData.$calendarList.find('> li'),
//                  $first = $listItems.first(),
//                  newCalendarTop, addItem;
//
//              sectionData.calendarItemIndex++;
//              if (sectionData.calendarItemIndex === calendarData.length) {
//                sectionData.calendarItemIndex = 0;
//              }
//
//              newCalendarTop = self.calcCalendarTop(id);
//
//              if ( (sectionData.calendarItemIndex + sectionData.itemDisplayQty) > calendarData.length) {
//                addItem = calendarData[(sectionData.calendarItemIndex - 1 + sectionData.itemDisplayQty) % sectionData.itemDisplayQty].el.clone();
//              } else {
//                addItem = calendarData[sectionData.calendarItemIndex - 1 + sectionData.itemDisplayQty].el.clone();
//              }
//
//              addItem.css('top', sectionData.$calendarList.height());
//              sectionData.$calendarList.append(addItem);
//
//              setTimeout(function() {
//                  $first.addClass('calendar-pop-item').css('top', -1 * $first.outerHeight());
//
//                  for (var i = 1, len = $listItems.length; i < len; i++) {
//                    $($listItems[i]).css('top', newCalendarTop);
//                    newCalendarTop += $($listItems[i]).outerHeight();
//                  }
//
//                  addItem.css('top', newCalendarTop);
//              }, 0);
//
//              setTimeout(function() {
//                  $first.remove();
//
//                  // check if it's the end of the calendar cycle
//                  if ( (sectionData.calendarItemIndex + sectionData.itemDisplayQty) === calendarData.length ) {
//                    if (!preventTrigger) {
//                      setTimeout(function() {
//                          $el.trigger('endanimation', [id]);
//                          sectionData.calendarItemIndex = 0;
//                      }, itemScrollTime * 2);
//                    }
//
//                  } else {
//                    setTimeout(animateCalendar, itemScrollTime);
//                  }
//
//              }, self.animationTime);
//          }
//
//          setTimeout(animateCalendar, itemScrollTime);
//      }
//
//      if (calendarData.length > maxCalendarItems) {
//        startCalendar();
//
//      } else if (!preventTrigger) {
//        setTimeout(function() {
//          $el.trigger('endanimation', [id]);
//        }, calendarData.length * itemScrollTime);
//      }
//  }
};