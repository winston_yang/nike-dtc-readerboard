PS.sections.slide = {
    defaultDuration: 1000,
    defaultStaticEventDuration: 4500,
    loopCycle: {},
    currentEvent: 0,
    currentEventStep: 0,
    totalEventStep: 3,
    MAX_EVENT: 3,
    endOfEventLoop: false,
    intvEventStep: {},
    render: function(id) {
        var sectionData,
            content = {},
            childrenItem = [],
            $el, i, rows = {}, rowText,_textStyle, eventList = [], self = this;

        sectionData = PS.sections.getSectionData(id);

        if (!sectionData) {
            return false;
        }

        if (sectionData.rowEvents.length) {
            for (i = 0; i < PS.sections.calendar.events.length; i++) {

                if ((sectionData.rowEvents[0].eventType === 'NRC' && PS.sections.calendar.events[i].type.id === 2)
                    || (sectionData.rowEvents[0].eventType === 'NTC' && PS.sections.calendar.events[i].type.id === 3))
                {
                    eventList.push(PS.sections.calendar.events[i]);
                    if (eventList.length === self.MAX_EVENT) {
                        break;
                    }
                }

            }

            for (i = 0; i < sectionData.rowEvents.length; i++) {
                sectionData.rowEvents[i].events = eventList;
                childrenItem.push(sectionData.rowEvents[i]);
            }
        }

        if (sectionData.rowTexts) {
            for (i = 0; i < sectionData.rowTexts.length; i++) {
                rowText = sectionData.rowTexts[i];
                _textStyle = '';
                if (rowText.fontSize) {
                    _textStyle += 'font-size:' + rowText.fontSize + ';'
                }
                if (rowText.color) {
                    _textStyle += 'color:' + rowText.color + ';'
                }
                rowText.textStyle = _textStyle;
                childrenItem.push(rowText);
            }
        }

        if (sectionData.rowMedias) {
            for (i = 0; i < sectionData.rowMedias.length; i++) {
                if (PS.data.config.asseturl && sectionData.rowMedias[i].filename.indexOf('http') === -1) {
                    sectionData.rowMedias[i].filename = PS.data.config.asseturl + sectionData.rowMedias[i].filename;

                } else if (sectionData.rowMedias[i].filename.indexOf('http') === -1) {
                    sectionData.rowMedias[i].filename = '../assets/img/' + sectionData.rowMedias[i].filename;
                }
                childrenItem.push(sectionData.rowMedias[i]);
            }
        }

        childrenItem.sort(function(a, b){return a.rowOrder-b.rowOrder});

        if (sectionData.landscapeimage && sectionData.landscapeimage.length) {
            content.backgroundImage = sectionData.landscapeimage[0];
        }


        $el = $(sectionData.template({
            content: content,
            sectionid: sectionData.sectionid
        }));

        //add row item
//        for (i = 0; i < childrenItem.length; i++) {
//            $el.append(PS.templates[childrenItem[i].type]({
//                content:childrenItem[i],
//                eventHashtag: childrenItem[i].eventHashtag
//            }));
//        }

        for (i = 0; i < childrenItem.length; i++) {
            $el.append(PS.templates[childrenItem[i].type]({
                content:childrenItem[i]
            }));
        }

        for (i = 1; i <= PS.data.config.numberOfRows; i++) {
            if ($el.find('.slide__row-order-' + i).length > 1) {
                $el.find('.slide__row-order-' + i).addClass('slide__row-multiple');
                this.loopCycle[i] = 0;
            }
        }



        return $el;
    },
    animateEvents: function(id) {
        var  $el = PS.sections.getEl(id), self = this,
            sectionData = PS.sections.getSectionData(id),
            totalEvents = sectionData.rowEvents[0].events.length-1;

        if ($el == undefined) {
            return;
        }

        $el.find('.event-list-item').hide();
        $el.find('.event-static:eq(' + self.currentEvent + ')').find('.event-list-item:eq(' + self.currentEventStep + ')').show();
        //$el.find('.event-static .event-list-item').show();

        //animate events for static version
        if (self.currentEvent === totalEvents && self.currentEventStep === self.totalEventStep) {
            //reach last event and event step
            self.currentEvent = 0;
            self.currentEventStep = 0;

            self.endOfEventLoop = true;

        } else if (self.currentEventStep === self.totalEventStep) {
            //reach last event step, go to next event
            self.currentEvent++;
            self.currentEventStep = 0;
        } else {
            if (self.currentEventStep < self.totalEventStep) {
                //next event step
                self.currentEventStep++;
            }
        }

        self.intvEventStep = setTimeout(function() {
                self.animateEvents(id);
            }, self.defaultStaticEventDuration);
    },

    nextSection: function(id) {
        var $el = PS.sections.getEl(id), self = this;

        if (!self.endOfEventLoop && PS.data.config.eventAnimation === 'non-animate') {

            setTimeout(function() {
                self.nextSection(id);
            }, 100);

            return;
        }
        $el.trigger('endanimation', [id]);
    },

    //refresh
    animate: function(id, duration, preventTrigger) {
        var $el = PS.sections.getEl(id), sectionData = PS.sections.getSectionData(id),
        self = this;

        duration = duration || this.defaultDuration;

        //alternate rows that has same row number.
        for (i = 1; i <= PS.data.config.numberOfRows; i++) {
            var target = $el.find('.slide__row-order-' + i);

            if (target.length > 1) {
                target.removeClass('active');
                $(target[this.loopCycle[i]]).addClass('active');
                this.loopCycle[i]++;
                if (this.loopCycle[i] > target.length) {
                    this.loopCycle[i] = 0;
                }
            }
        }

        if (PS.data.config.eventAnimation === 'non-animate' && sectionData.rowEvents[0] && sectionData.rowEvents[0].events.length) {
            self.currentEvent = 0;
            self.currentEventStep = 0;
            self.endOfEventLoop = false;
            clearTimeout(self.intvEventStep);
            self.animateEvents(id);
        }


        if (!preventTrigger) {
            setTimeout(function() {
                self.nextSection(id);
            }, duration);
        }
    }
};