PS.sections.layoutTwoContainers = {
  render: function(id) {
      var sectionData = PS.sections.getSectionData(id),
          content,
          $el;

      if (!sectionData) {
        return false;
      }

      $el = $(sectionData.template({
        sectionid: sectionData.sectionid
      }));

      $el.find('.container1').append(PS.sections.render(sectionData.container1));
      //$el.find('.container2').append(PS.sections.render(sectionData.container2));

      $el.find('> .containers-wrapper').height($('body').height());
      return $el;
  },

  animate: function(id, duration) {
      var sectionData = PS.sections.getSectionData(id),
          $el = sectionData.$el,
          cont1Data = PS.sections.getSectionData(sectionData.container1);
          //cont2Data = PS.sections.getSectionData(sectionData.container2);

      PS.sections.setImageBackground(cont1Data);
      //PS.sections.setImageBackground(cont2Data);

      // stop propagation of children sections "endanimation" event
      $el.off('endanimation');
      $el.on('endanimation', function(ev, containedId) {
          ev.preventDefault();
          ev.stopPropagation();

          if (duration) {
            return;
          }

          if (containedId === sectionData.control) {
            setTimeout(function() {
                $el.parent().trigger('endanimation', [id]);
            }, 0);
          }
      });

      cont1Data.jsController.animate(cont1Data.sectionid, cont1Data.duration, cont1Data.sectionid !== sectionData.control);
      //cont2Data.jsController.animate(cont2Data.sectionid, cont2Data.duration, cont2Data.sectionid !== sectionData.control);

      if (duration) {
        setTimeout(function() {
            $el.off('endanimation');
            $el.trigger('endanimation', [id]);

            cont1Data.jsController.afterAnimation && cont1Data.jsController.afterAnimation();
            //cont2Data.jsController.afterAnimation && cont2Data.jsController.afterAnimation();
        }, duration);
      }
  },

  canPlay: function(id) {
      var sectionData = PS.sections.getSectionData(id),
          controlData = PS.sections.getSectionData(sectionData.control),
          play = true;

      if (controlData.jsController.canPlay) {
        play = controlData.jsController.canPlay(controlData.sectionid);
      }

      return play;
  }
};