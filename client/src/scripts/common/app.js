/**
 * PS modules namespace declaration
 * @namespace
 */
PS.modules = {};

/**
 * PS section modules namespace declaration
 * @namespace
 */
PS.sections = {};

/**
 * PS modules stores namespace declaration
 * used for module extension and custom initialization
 * @namespace
 */
PS.stores = {};

/**
 * store modules compiled handlebars templates
 * @namespace
 */
PS.templates = {};

/**
 * main app namespace
 * @namespace
 */
PS.main = {
  // PS.main.init will be executed in all store instances
  init: function () {
      this.$sectionsContainer = $('.sections-container');
      console.log('main init');

      PS.modules.sequenceManager.init(function() {

      });
      // PS.modules.socketComm.init();
  }
};

$(function() {
    PS.main.init();
});