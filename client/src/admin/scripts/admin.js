$(function() {
    var $clientsList = $('.client-list');

    var socket = io.connect('http://' + window.location.host);

    socket.emit('getclients');

    socket.on('clientslist', function(data) {
        renderList(data.clients);
    });

    var renderList = function(clients) {
        $clientsList.empty();
        
        for (var i = 0; i < clients.length; ++i) {
          if (clients[i].isNano) {
            $clientsList.append('<li><div>ID: ' + clients[i].id + '</div><div>EXPERIENCE ID: ' + clients[i].experienceId + '</div><a href="#" data-experienceid="' + clients[i].experienceId + '" data-nano="true" class="reload">reload</a></li>')

          } else {
            $clientsList.append('<li><div>ID: ' + clients[i].id + '</div><div>EXPERIENCE ID: ' + clients[i].experienceId + '</div><a href="#" data-experienceid="' + clients[i].experienceId + '" class="reload">reload</a></li>');
          }
        }
    }

    // $clientsList.on('click', 'li a.refresh', function(ev) {
    //     var $el = $(this);

    //     ev.preventDefault();

    //     socket.emit('refreshexperience', {
    //       experienceId: $el.data('experienceid'),
    //       isNano: $el.data('nano')
    //     });
    // });

    $clientsList.on('click', 'li a.reload', function(ev) {
        var $el = $(this);

        ev.preventDefault();

        socket.emit('reloadexperience', {
          experienceId: $el.data('experienceid'),
          isNano: $el.data('nano')
        });
    });
});