var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    jshint = require('gulp-jshint'),
    rimraf = require('gulp-rimraf'),
    stylish = require('jshint-stylish'),
    concat = require('gulp-concat-sourcemap'),
    tap = require('gulp-tap'),
    newer = require('gulp-newer'),
    handlebars = require('gulp-handlebars'),
    livereload = require('gulp-livereload'),
    defineModule = require('gulp-define-module'),
    gzip = require("gulp-gzip"),
    s3 = require("gulp-s3"),
    fs = require('fs'),
    shell = require('gulp-shell'),
    config = require('./config');

gulp.task('clean', function() {
    return gulp.src([config.staticDest + '/**/*.*', config.staticDest + '/**/*'], {read: false})
        .pipe(rimraf({force: true}));
});

gulp.task('sass', function() {
    return sass('src/styles/', {style: 'expanded'})
        .pipe(gulp.dest(config.cssStaticDest));
});

gulp.task('styles', ['sass'], function() {
    return gulp.src([config.cssStaticDest + '/**/*.css'])
        .pipe(autoprefixer({
          browsers: ["last 20 versions"]
        }))
        .pipe(gulp.dest(config.cssStaticDest))
        .pipe(livereload());
});

gulp.task('jshint', function() {
    return gulp.src(['src/scripts/**/*.js', '!src/scripts/common/libs/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

gulp.task('copy-dev-scripts', function() {
    return gulp.src(['src/scripts/**/*.js'])
        .pipe(newer(config.jsStaticDest))
        .pipe(gulp.dest(config.jsStaticDest + '/src/scripts'));
});

gulp.task('main-scripts', ['templates', 'jshint'], function() {
    return gulp.src(['src/scripts/common/libs/*.js', 'src/scripts/common/**/*.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest(config.jsStaticDest))
        .pipe(livereload());
});

gulp.task('experience-scripts', function() {
    return gulp.src(['src/scripts/experiences/*'])
        .pipe(tap(function(file, t) {
            return gulp.src([file.path + '/**/*.js'])
                .pipe(concat('experience.js', {
                  sourceRoot: '../'
                }))
                .pipe(gulp.dest(config.jsStaticDest + '/' + file.relative + '/'));
        }))
        .pipe(livereload());
});

gulp.task('admin-scripts', function() {
    return gulp.src(['src/admin/scripts/libs/*.js', 'src/admin/scripts/**/*.js'])
        .pipe(concat('admin.js'))
        .pipe(gulp.dest(config.jsStaticDest))
        .pipe(livereload());
});
gulp.task('admin-sass', function() {
    return sass('src/admin/styles/', {style: 'expanded'})
        .pipe(gulp.dest(config.cssStaticDest));
});
gulp.task('admin-styles', ['admin-sass'], function() {
    return gulp.src([config.cssStaticDest + '/admin.css'])
        .pipe(autoprefixer({
          browsers: ["last 2 versions"]
        }))
        .pipe(gulp.dest(config.cssStaticDest))
        .pipe(livereload());
});

gulp.task('templates', function() {
    return gulp.src('src/templates/**/*.hbs')
        .pipe(handlebars({
          handlebars: require('handlebars')
        }))
        .pipe(defineModule('plain', {
          wrapper: 'PS.templates["<%= name %>"] = <%= handlebars %>'
        }))
        .pipe(concat('templates.js'), {

        })
        .pipe(gulp.dest('src/scripts/common'));
});

gulp.task('static-assets', function() {
    return gulp.src('src/static/**/*')
        .pipe(newer(config.assetsStaticDest))
        .pipe(gulp.dest(config.assetsStaticDest));
});

gulp.task('watch', ['static-assets', 'copy-dev-scripts', 'main-scripts', 'experience-scripts', 'admin-scripts', 'styles', 'admin-styles'], function() {
    livereload.listen();
    gulp.watch('src/admin/scripts/**/*.js', ['admin-scripts']);
    gulp.watch('src/admin/styles/**/*.scss', ['admin-styles']);    

    gulp.watch('src/scripts/**/*.js', ['main-scripts', 'experience-scripts']);
    gulp.watch('src/styles/**/*.scss', ['styles']);
    gulp.watch('src/templates/**/*.hbs', ['templates']);
    gulp.watch('src/static/**/*', ['static-assets']);
});

// gulp.task('development', ['clean'], function() {
gulp.task('development', function() {
    gulp.start('watch');
});


gulp.task('s3-push', function() {
  aws = JSON.parse(fs.readFileSync('./config.json'))
  gulp.src(config.staticDest + '/**')
      .pipe(s3(aws));
});




