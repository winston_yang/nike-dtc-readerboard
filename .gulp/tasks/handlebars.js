var gulp        = require('gulp'),
    handlebars  = require('gulp-handlebars'),
    wrap        = require('gulp-wrap'),
    declare     = require('gulp-declare'),
    concat      = require('gulp-concat'),
    config      = require('../config').templates;

gulp.task('templates', function() {
    return gulp.src(config.src)
        .pipe(handlebars())
        .pipe(wrap('Handlebars.template(<%= contents %>)'))
        .pipe(declare({
          root: 'exports',
          noRedeclare: true, // Avoid duplicate declarations 
          processName: function(filePath) {
              return declare.processNameByPath(filePath.replace('templates/', ''))
          }
        }))
        .pipe(concat('index.js'))
        .pipe(wrap('var Handlebars = require("handlebars");\n <%= contents %>'))
        .pipe(gulp.dest(config.dest));
});