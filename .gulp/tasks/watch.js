'use strict';
var gulp        = require('gulp'),
    config      = require('../config'),
    livereload  = require('gulp-livereload');

gulp.task('watch', function() {
    gulp.watch('./.config/client.json', ['client-config']);
    gulp.watch(config.assets.src, ['assets']);
    gulp.watch(config.templates.src, ['templates']);
    gulp.watch(config.server.src, ['lint-server']);
    gulp.watch(config.scripts.src, ['lint-scripts']);
    gulp.watch(config.sass.src + '**/*', ['sass']);

    livereload.listen();
    gulp.watch([config.global.dest+'/**']).on('change', livereload.changed);
});
