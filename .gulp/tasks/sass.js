var gulp        = require('gulp'),
    sass        = require('gulp-ruby-sass'),
    sourcemaps  = require('gulp-sourcemaps'),
    config      = require('../config').sass;

gulp.task('sass', function() {
    return sass(config.src)
      .on('error', function(err) {
          console.error('SASS error', err);
      })
      .pipe(gulp.dest(config.dest));
});