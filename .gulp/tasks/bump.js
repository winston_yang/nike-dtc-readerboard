var gulp    = require('gulp'),
    bump    = require('gulp-bump'),
    minimist= require('minimist'),
    pkgSrc  = './package.json';

gulp.task('bump', ['bump:patch'], function() {
});

gulp.task('bump:patch', function() {
    return gulp.src(pkgSrc)
      .pipe(bump({type: 'patch'}))
      .pipe(gulp.dest('./'));
});

gulp.task('bump:minor', function() {
    return gulp.src(pkgSrc)
      .pipe(bump({type: 'minor'}))
      .pipe(gulp.dest('./'));
});

gulp.task('bump:major', function() {
    return gulp.src(pkgSrc)
      .pipe(bump({type: 'major'}))
      .pipe(gulp.dest('./'));
})

gulp.task('bump:pre', function() {
    var options = minimist(process.argv),
        label = options.label || 'rc';

    return gulp.src(pkgSrc)
      .pipe(bump({
        type: 'prerelease',
        preid: label
      }))
      .pipe(gulp.dest('./'));
});

gulp.task('bump-client', ['bump-client:patch'], function() {
});

gulp.task('bump-client:patch', function() {
    return gulp.src(pkgSrc)
      .pipe(bump({
        key: 'clientVersion',
        type: 'patch'
      }))
      .pipe(gulp.dest('./'));
});

gulp.task('bump-client:minor', function() {
    return gulp.src(pkgSrc)
      .pipe(bump({
        key: 'clientVersion',
        type: 'minor'
      }))
      .pipe(gulp.dest('./'));
});

gulp.task('bump-client:major', function() {
    return gulp.src(pkgSrc)
      .pipe(bump({
        key: 'clientVersion',
        type: 'major'
      }))
      .pipe(gulp.dest('./'));
})

gulp.task('bump-client:pre', function() {
    var options = minimist(process.argv),
        label = options.label || 'rc';

    return gulp.src(pkgSrc)
      .pipe(bump({
        key: 'clientVersion',
        type: 'prerelease',
        preid: label
      }))
      .pipe(gulp.dest('./'));
});