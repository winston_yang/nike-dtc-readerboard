var gulp    = require('gulp');
var config  = require('../config');
var changed = require('gulp-changed');

gulp.task('copy-to-assets', function () {
    return gulp.src( config.assets.src )
      .pipe(changed(config.assets.src))
      .pipe(gulp.dest( config.assets.dest ));
});

gulp.task('assets', ['copy-to-assets'], function() {
    return gulp.src(config.assets.dest + 'fonts/**')
      .pipe(gulp.dest(config.sass.dest + '/fonts'));
});