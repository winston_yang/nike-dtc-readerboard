var gulp    = require('gulp'),
    config  = require('../config'),
    nodemon = require('gulp-nodemon'),
    env     = process.env.NODE_ENV || 'local';

gulp.task('server', function() {
    nodemon({
      script: config.server.script,
      watch: config.server.src,
      env: {'NODE_ENV': env}
    });
});