'use strict';

var gulp          = require('gulp');
var config        = require('../config').global;
var gutil         = require('gulp-util');
var fs            = require('fs');
var path          = require('path');
var clientConfig  = require(__dirname + '/../../.config/client.json');
var env           = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
var util          = require('util');

gulp.task('client-config', function() {
    var envConfig = {};

    // get defaults
    util._extend(envConfig, clientConfig.defaults);
    
    // get env specific config
    util._extend(envConfig, clientConfig[env]);

    envConfig.env = env;
    fs.writeFileSync(__dirname + '/../../' + config.src + '/scripts/client.json', JSON.stringify(envConfig));
});