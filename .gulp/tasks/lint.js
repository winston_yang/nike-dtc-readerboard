var gulp    = require('gulp'),
    jshint  = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    config  = require('../config');

gulp.task('lint-server', function() {
    return gulp.src(config.server.src)
      .pipe(jshint())
      .pipe(jshint.reporter(stylish));
});

gulp.task('lint-scripts', function() {
    return gulp.src(config.scripts.src)
      .pipe(jshint({
        lookup: false
      }))
      .pipe(jshint.reporter(stylish));
});