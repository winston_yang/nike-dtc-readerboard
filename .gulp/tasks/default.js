/*
 * default.js
 * ==========
 * 
 * Our default gulp task includes basic running instructions that
 * might be helpful if someone just runs "gulp" in the command line.
 */

var gulp = require('gulp');

gulp.task('default', function(){
  console.log('\n\n   Welcome to Nike MyCity Readerboard.\n');
  console.log('\n\n   To start the server, use the "gulp dev" command.\n');
  console.log('\n');
});

gulp.task('help', ['default']);