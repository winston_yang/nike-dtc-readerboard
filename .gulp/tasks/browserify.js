'use strict';

var watchify    = require('watchify'),
    browserify  = require('browserify'),
    gulp        = require('gulp'),
    gulpif      = require('gulp-if'),
    source      = require('vinyl-source-stream'),
    buffer      = require('vinyl-buffer'),
    gutil       = require('gulp-util'),
    handleErrors= require('./util/handleErrors'),
    sourcemaps  = require('gulp-sourcemaps'),
    assign      = require('lodash.assign'),
    env         = process.env.NODE_ENV || 'local',
    localEnv    = env === "local",
    config      = require('../config').scripts;

// add custom browserify options here
var customOpts = {
  entries: config.entries,
  debug: localEnv ? true : false
};

var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts)); 

// add transformations here
// i.e. b.transform(coffeeify);

gulp.task('js', bundle); // so you can run `gulp js` to build the file

if (localEnv) {
  b.on('update', bundle); // on any dep update, runs the bundler
  b.on('log', gutil.log); // output build logs to terminal
  b.on('error', handleErrors);
}

function bundle() {
    return b.bundle()
      // log errors if they happen
      .on('error', gutil.log.bind(gutil, 'Browserify Error'))
      .pipe(source('app.js'))
      // optional, remove if you don't need to buffer file contents
      .pipe(buffer())
      // optional, remove if you dont want sourcemaps
      .pipe( gulpif(localEnv, sourcemaps.init({loadMaps: true})) ) // loads map from browserify file
         // Add transformation tasks to the pipeline here.
      .pipe( gulpif(localEnv, sourcemaps.write('./')) ) // writes .map file
      .pipe(gulp.dest(config.dest));
}