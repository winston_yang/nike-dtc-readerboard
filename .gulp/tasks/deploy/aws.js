'use strict';

// AWS credentials are loaded from aws credentials file ~/.aws/credentials
// or from env variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY

var gulp          = require('gulp'),
    AWS           = require('aws-sdk'),
    Archiver      = require('archiver'),
    fs            = require('fs'),
    Q             = require('q'),
    pkg           = require(__dirname + '/../../../package.json'),
    env           = process.env.NODE_ENV ? process.env.NODE_ENV : 'development',
    pkgName       = pkg.name + '-' + env + '-v' + pkg.version + '-' + new Date().getTime(),
    awsConfig     = require(__dirname + '/../../../.config/aws.json')[env],
    s3AssetConfig = require(__dirname + '/../../../.config/aws.json').mycity,
    args          = require('yargs').argv,
    url           = require('url'),
    http          = require('http'),
    Assets        = require('./utils/assets'),
    recursiveDir  = require('recursive-readdir'),
    path          = require('path'),
    s3            = new AWS.S3();

var ENVIRONMENT_HEALTH_CHECK = 30000;

AWS.config.loadFromPath(__dirname + '/../../../.config/aws-credentials.json');

gulp.task('deploy-assets', ['deploy-styles', 'deploy-scripts'], function() {
    var deferred = Q.defer();

    if (args.setLatest) {
      s3.upload({
        Bucket: s3AssetConfig.Bucket,
        Key: 'readerboard/latest.json',
        Body: JSON.stringify({
          version: pkg.clientVersion
        }),
        ContentType: 'application/json'
      
      }, function(err, data) {
          deferred.resolve();
      });
    
    } else {
      deferred.resolve();
    }

    return deferred.promise;
});

gulp.task('deploy-styles', ['sass'], function() {
    var deferred = Q.defer();

    recursiveDir(__dirname + '/../../../build/readerboard/styles/dev', function(err, files) {
        var uploads = files.length,
            p       = path.normalize(__dirname + '/../../../build/readerboard/styles/dev');

        for (var i = 0, len = files.length; i < len; ++i) {
          console.log('>> uploading', files[i]);
          Assets.upload({
            filename: files[i],
            fileKey: 'readerboard/styles/' + pkg.clientVersion + files[i].replace(p, ''),
            filePath: files[i],
            mimeType: files[i].indexOf('css') ? 'text/css' : ''
          })
            .then(function(filename) {
                console.log('>>>> ' + filename + ' uploaded');
            })
            .fail(function(err) {
                console.error('>>>> error uploading ' + filename, err);
                deferred.reject(err);
            })
            .fin(function() {
                uploads--;

                if (uploads === 0) {
                  deferred.resolve();
                }
            });
        }
    });

    return deferred.promise;
});

gulp.task('deploy-scripts', ['client-config', 'templates', 'js'], function() {
    var deferred = Q.defer();

    recursiveDir(__dirname + '/../../../build/readerboard/scripts/dev', function(err, files) {
        var uploads = files.length,
            p       = path.normalize(__dirname + '/../../../build/readerboard/scripts/dev');

        for (var i = 0, len = files.length; i < len; ++i) {
          console.log('>> uploading', files[i]);
          Assets.upload({
            filename: files[i],
            fileKey: 'readerboard/scripts/' + pkg.clientVersion + files[i].replace(p, ''),
            filePath: files[i],
            mimeType: 'application/javascript'
          })
            .then(function(filename) {
                console.log('>>>> ' + filename + ' uploaded');
            })
            .fail(function(err) {
                console.error('>>>> error uploading ' + filename, err);
                deferred.reject(err);
            })
            .fin(function() {
                uploads--;

                if (uploads === 0) {
                  deferred.resolve();
                }
            });
        }
    });

    return deferred.promise;
});


// prepare .zip package to deploy to S3 bucket
// zip /buld and /src files
gulp.task('prepare-zip', ['build'], function() {
    var archive = Archiver('zip'),
        output = fs.createWriteStream(__dirname + '/../../../' + pkgName + '.zip'),
        deferred = Q.defer();

    output.on('close', function() {
        console.log('Package ZIP created: ' + pkgName + '.zip');
        deferred.resolve();
    });

    archive.on('error', function(err) {
        throw err;
    });

    archive.pipe(output);

    awsConfig.s3.zipContents.forEach(function(folder) {
        archive.directory(__dirname + '/../../../' + folder, folder);
    });
    archive.file(__dirname + '/../../../package.json', {name: 'package.json'});

    archive.finalize();

    return deferred.promise;
});

// push zip package to S3 bucket
gulp.task('s3-push', ['prepare-zip'], function() {
    var s3 = new AWS.S3(),
        stream = fs.createReadStream(__dirname + '/../../../' + pkgName + '.zip'),
        deferred = Q.defer(),
        uploadCount = 0;

    console.log("Pushing deploy package to S3 Bucket: " + awsConfig.s3.Bucket);

    s3.upload({
      Bucket: awsConfig.s3.Bucket,
      Key: pkgName + '.zip',
      Body: stream
    
    }, function(err, data) {
        if (err) {
          console.log(err, err.stack);

          // delete zip package
          fs.unlinkSync(__dirname + '/../../../' + pkgName + '.zip');

          deferred.reject();

        } else {
          console.log("100% uploaded");
          fs.unlinkSync(__dirname + '/../../../' + pkgName + '.zip');

          deferred.resolve();
        }
    })
      .on('httpUploadProgress', function(progress, response) {
          if (uploadCount % 25 === 0) {
            console.log( parseFloat(progress.loaded / progress.total * 100).toFixed(2) + "% uploaded" )
          }
          uploadCount++;
      })
      .on('httpError', function(err) {
          console.log('Error uploading to S3', err);

          fs.unlinkSync(__dirname + '/../../../' + pkgName + '.zip');
          deferred.reject();
      });
  
    return deferred.promise;
});

function createVersion(params) {
    var deferred = Q.defer(),
        eb = new AWS.ElasticBeanstalk();

    eb.createApplicationVersion(params, function(err, data) {
        if (err) {
          console.log("ERROR creating application version.", err);

          deferred.reject();

        } else {
          console.log("Application version created.");
          deferred.resolve();
        }
    });

    return deferred.promise;
}

// create ElascticBeanstalk app version with the S3 deployed package
gulp.task('create-eb-version', ['s3-push'], function() {
    var eb = new AWS.ElasticBeanstalk(),
        ebConfig = awsConfig.eb,
        ebParams = {};

    ebParams.ApplicationName = ebConfig.ApplicationName;
    ebParams.AutoCreateApplication = ebConfig.AutoCreateApplication;

    ebParams.SourceBundle = {
      S3Bucket: awsConfig.s3.Bucket,
      S3Key: pkgName + '.zip'
    };
    ebParams.VersionLabel = pkgName;
    ebParams.Description = env + " autodeploy - " + pkgName;

    console.log("Creating application version: " + ebParams.Description);

    return createVersion(ebParams);
});

// update environment
gulp.task('update-eb', ['create-eb-version'], function() {
    var eb = new AWS.ElasticBeanstalk(),
        ebConfig = awsConfig.eb,
        ebParams = {},
        deferred = Q.defer(),
        healthInterval;

    ebParams.EnvironmentName = ebConfig.EnvironmentName;
    ebParams.VersionLabel = pkgName;

    console.log(ebParams.EnvironmentName + " environment update is starting.");

    eb.updateEnvironment(ebParams, function(err, data) {
        if (err) {
          console.log(err, err.stack);
          deferred.reject();
        
        } else {
          console.log('UPDATING ENVIRONMENT: ', ebConfig.EnvironmentName);

          environmentCheck(ebConfig)
          .then(function() {
              console.log('ENVIRONMENT UPDATE COMPLETED.');

              setTimeout(function() {
                  checkHealth(awsConfig.url)
                  .then(function() {
                      deferred.resolve();
                  
                  }, function() {
                      deferred.reject();
                  });
              }, 10000);
          
          }, function() {
              deferred.reject();
          });
        }
    });

    return deferred.promise;
});

function checkHealth(checkUrl, healthEnv) {
    var urlObj = url.parse(checkUrl + 'health/check'),
        deferred = Q.defer(),
        failCount = 0;

    healthEnv = healthEnv ? healthEnv : env;

    function makeRequest() {
        var req = http.request({
          host: urlObj.hostname,
          path: urlObj.pathname,
          port: urlObj.port,
          method: 'GET'
        
        }, function(res) {
            res.setEncoding('utf8');
            res.on('data', function(data) {
              var parsedResponse;

              try {
                parsedResponse = JSON.parse(data);
                if (parsedResponse.status && parsedResponse.status === "ok") {
                  deferred.resolve();
                
                } else {
                  deferred.reject();
                }
              
              } catch(e) {
                failCount++;

                if (failCount < 3) {
                  makeRequest();
                
                } else {
                  deferred.reject();
                }
              }
            });
        });
        
        req.on('error', function() {
            failCount++;
            
            if (failCount < 3) {
              makeRequest();
            
            } else {
              deferred.reject();  
            }
        });
        req.end();
    }
    
    makeRequest();

    return deferred.promise;
}

function environmentCheck(config) {
    var eb = new AWS.ElasticBeanstalk(),
        deferred = Q.defer();

    function check() {
        eb.describeEnvironments({
          ApplicationName: config.ApplicationName,
          EnvironmentNames: [
            config.EnvironmentName
          ]
        
        }, function(err, data) {
            if (err) {
              console.log('ERROR requesting environment status: ', err);
              deferred.reject();
              return;
            }

            if (data.Environments && data.Environments.length && data.Environments[0].Status === "Ready") {
              deferred.resolve();

            } else if (!data.Environments || !data.Environments.length) {
              console.log('ERROR requesting environment status: no environment returned.');
              deferred.reject();
            
            } else {
              setTimeout(check, ENVIRONMENT_HEALTH_CHECK);  
            }
        });
    }
    
    check();

    return deferred.promise;
}

// update environment from another one
gulp.task('update-from-eb', function() {
    var eb = new AWS.ElasticBeanstalk(),
        from = args.s || args.src,
        to = args.d || args.dest,
        deferred = Q.defer(),
        fromConfig,
        toConfig;
    
    fromConfig = require(__dirname + '/../../../.config/aws.json')[from];
    toConfig = require(__dirname + '/../../../.config/aws.json')[to];

    eb.describeEnvironments({
      ApplicationName: fromConfig.eb.ApplicationName,
      EnvironmentNames: [
        fromConfig.eb.EnvironmentName
      ]
    
    }, function(err, data) {
        if (err) {
          console.log('ERROR getting info for ' + fromConfig.eb.EnvironmentName + '.' );
          deferred.reject();
          return;
        
        }

        if (data.Environments && data.Environments.length && data.Environments[0].Status === "Ready") {
          createVersion({
            ApplicationName: toConfig.eb.ApplicationName,
            AutoCreateApplication: toConfig.eb.AutoCreateApplication,
            SourceBundle: {
              S3Bucket: toConfig.s3.Bucket,
              S3Key: data.Environments[0].VersionLabel + '.zip'
            },
            VersionLabel: 'from-' + data.Environments[0].VersionLabel,
            Description: 'Package from ' + from
          
          }).then(function() {
              console.log(toConfig.eb.EnvironmentName + " environment update is starting.");

              eb.updateEnvironment({
                EnvironmentName: toConfig.eb.EnvironmentName,
                VersionLabel: data.Environments[0].VersionLabel
              
              }, function(err, data) {
                  if (err) {
                    console.log(err, err.stack);
                    deferred.reject();
                  
                  } else {
                    console.log('UPDATING ENVIRONMENT: ', to);

                    environmentCheck(toConfig.eb)
                    .then(function() {
                        console.log('ENVIRONMENT UPDATE COMPLETED.');

                        setTimeout(function() {
                            checkHealth(toConfig.url)
                            .then(function() {
                                deferred.resolve();
                            
                            }, function() {
                                deferred.reject();
                            });
                        }, 10000);
                    
                    }, function() {
                        deferred.reject();
                    });
                  }
              });
          });
        }
    });
    
    return deferred.promise;
});