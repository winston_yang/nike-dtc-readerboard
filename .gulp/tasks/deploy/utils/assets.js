var Q             = require('q'),
    AWS           = require('aws-sdk'),
    fs            = require('fs'),
    s3AssetConfig = require(__dirname + '/../../../../.config/aws.json').mycity,
    s3            = new AWS.S3();

module.exports = {
  upload: function(params) {
      var deferred = Q.defer(),
          fileStream = fs.createReadStream(params.filePath);

      s3.upload({
        Bucket: s3AssetConfig.Bucket,
        Key: params.fileKey,
        Body: fileStream,
        ContentType: params.mimeType
      
      }, function(err, data) {
          if (err) {
            deferred.reject(err);
          
          } else {
            deferred.resolve(params.filename);
          }
      });

      return deferred.promise;
  }
};