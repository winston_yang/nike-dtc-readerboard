'use strict';

var path = require('path');

var dest = './build';
var src = './src';

module.exports = {
  global: {
    dest: dest,
    src: src
  },

  sass: {
    src: src + '/styles/',
    dest: dest + '/readerboard/styles/dev'
  },

  server: {
    script: src + '/server/server.js',
    src: src + '/server/**/*.js'
  },

  scripts: {
    src: src + '/scripts/**/*.js',
    dest: dest + '/readerboard/scripts/dev',
    entries: [src + '/scripts/app.js']
  },

  clean: {
    dest: dest + '/**/*'
  },

  assets: {
    src: src + '/assets/**/*',
    dest: dest + '/assets/',
  },

  templates: {
    src: src + '/templates/**/*.hbs',
    dest: src + '/scripts/templates/'
  }
};
