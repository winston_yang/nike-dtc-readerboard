'use strict';

var express     = require('express'),
    app         = express(),
    path        = require('path'),
    fs          = require('fs'),
    favicon     = require('serve-favicon'),
    http        = require('http'),
    hbs         = require('express-hbs'),
    appPackage  = require(__dirname + '/../../package'),
    s3Mycity    = require(__dirname + '/../../.config/aws').mycity,
    request     = require('request'),
    server;

app.engine('hbs', hbs.express4());
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

app.locals.LOCAL_DEV = 'local' === app.get('env');

app.use(express.static(__dirname + '/../../build'));
app.use(favicon(path.join(__dirname, '../../build', 'assets', 'favicon.ico')));

var latestClientVer;

function getLatestClientVersion(cb) {
    request({
      url: 'http://' + s3Mycity.Endpoint + '/readerboard/latest.json',
      method: 'GET'
    
    }, function(error, response, body){
        var verJson;

        if(error) {
          console.log(error);
          cb();
      
        } else {
          verJson = JSON.parse(body);

          latestClientVer = verJson.version;
          console.log('> latest client ver set:', verJson.version);
          cb(latestClientVer);
        }
    });
}

app.get('/clearClient', function(req, res) {
    latestClientVer = false;

    res.send('ok');
});

app.get('/:version?', function(req, res) {
    if (!req.params.version) {
      if (!latestClientVer) {
        getLatestClientVersion(function(ver) {
            if (!ver) {
              ver = appPackage.version;
            }
            res.render('index', {
              s3Endpoint: s3Mycity.Endpoint,
              version: ver,
              title: appPackage.name
            });
        });  
      
      } else {
        res.render('index', {
          s3Endpoint: s3Mycity.Endpoint,
          version: latestClientVer,
          title: appPackage.name
        });
      }
      
    } else {
      res.render('index', {
        s3Endpoint: s3Mycity.Endpoint,
        version: req.params.version,
        title: appPackage.name
      });
    }
    
});

console.log('> starting server: ', process.env.NODE_ENV);

server = http.createServer(app);
server.listen(process.env.READERBOARD_PORT || 8081);
server.on('listening', function() {
    console.log('> server listening on port: ', this.address().port);
});