var clientConfig = require('../client');

var Service = {
  requestCalendar: function(param, cb) {
      var url =  param.url + param.calendarsources + '?from=' + param.from + '&to=' + param.to;

      $.ajax({
        url: url,
        dataType: 'jsonp',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa(clientConfig.mycityApiUser + ":" + clientConfig.mycityApiPass));
        }
      
      }).done(function(data) {
          console.log('data=' + data);
          cb(data);

      }).fail(function() {
          cb([]);
      });
  }
};

module.exports = Service;