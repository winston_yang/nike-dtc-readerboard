var clientConfig  = require('../client'),
    Q             = require('q'),
    templateData  = {},
    utils         = require('../utils'),
    idCount       = 0;

function genId() {
    return 'Nike' + idCount++;
}

function getConfigFile(data, cb) {
    var $config = $(data).find('template #configFile'),
        url = Nanonation.remote ? $config.attr('remoteUrl') : clientConfig.nanonationSrc + $config.attr('value');

    $.ajax({
      url: url + '?_=' + new Date().getTime(),
      dataType: 'json'
    })
      .done(function(data) {
          if (Nanonation.remote) {
            $.ajax({
              url: clientConfig.mycityApiUrl + 'nanonation/template/' + Nanonation.source + '/url',
              dataType: 'jsonp'
            
            }).done(function(urlData) {
                data.templateUrls = urlData;
                cb(data);
            });

          } else {
            cb(data);
          }
      }).
      fail(function() {

      });          
}

function processBackgroundImages($xml) {
    var $media = $xml.find('template > [name="[Media]"]'),
        images = {};

    for (var i = 0, len = $media.length; i < len; ++i) {
      images[$($media[i]).attr('guid')] = {
        eventtype: $($media[i]).find('> [id="eventtype"]').attr('value'),
        filename: $($media[i]).find('> [id="source"]').attr('value'),
        orientation: $($media[i]).find('> [id="orientation"]').attr('value'),
        name: $($media[i]).find('> [id="name"]').attr('value')
      };
    }

    return images;
}

function processFixedText($xml) {
    var $item = $xml.find('template > [name="[Row Text]"]'),
        $target,
        items = {},
        rowTexts = {};

    for (var i = 0, len = $item.length; i < len; ++i) {
        $target = $($item[i]);
        items[$target.attr('guid')] = {
            text: $target.find('> [id="text"]').attr('value'),
            fontSize: $target.find('> [id="font-size"]').attr('value'),
            fontWeight: $target.find('> [id="font-weight"]').attr('value'),
            alignment: $target.find('> [id="alignment"]').attr('value'),
            color: $target.find('> [id="color"]').attr('value'),
            name: $target.find('> [id="name"]').attr('value'),
            type: 'row-text',
            rowOrder: $target.find('> [id="row-order"]').attr('value')
        };
    }

    return items;
}

function processEvents($xml) {
    var $item = $xml.find('template > [name="[Row Events]"]'),
        $target,
        items = {};

    for (var i = 0, len = $item.length; i < len; ++i) {
        $target = $($item[i]);
        items[$target.attr('guid')] = {
          rowOrder: $target.find('> [id="row-order"]').attr('value'),
          eventType: $target.find('> [id="event-type"]').attr('value'),
          multiLine: $target.find('> [id="Multi-line"]').attr('value'),
          fontSize: $target.find('> [id="font-size"]').attr('value'),
          color: $target.find('> [id="color"]').attr('value'),
          // eventTemplate: $target.find('> [id="event-template"]').attr('value'),
          scrollDuration: $target.find('> [id="scroll-duration"]').attr('value'),
          eventHashtag: $target.find('> [id="event-hashtag"]').attr('value'),
          alignment: $target.find('> [id="alignment"]').attr('value'),
          type: 'row-event',
          name: $target.find('> [id="name"]').attr('value')
        };
    }

    return items;
}

function processMedia($xml) {
    var $item = $xml.find('template > [name="[Row Media]"]'),
        $target,
        items = {};

    for (var i = 0, len = $item.length; i < len; ++i) {
        $target = $($item[i]);
        relatedMedia = templateData.backgroundimages[$target.find('#background value').text()];
        items[$target.attr('guid')] = {
          rowOrder: $target.find('> [id="row-order"]').attr('value'),
          background:$target.find('> [id="row-order"]').attr('value'),
          type: 'row-media',
          filename: relatedMedia.filename,
          name: $target.find('> [id="name"]').attr('value')
        };
    }

    return items;
}

function processSection(type, $node, defaults, sectionId) {
    var result = $.extend({}, defaults);

    $node.children().each(function(i, child) {
        var $child = $(child),
            backgrounds = [],
            rowTexts = [],
            rowEvents = [],
            rowMedias = [];

        if (child.getAttribute('id') == 'background-image' && $child.children().length > 0) {
          // TODO check if background is an image or video
          $child.find('> values > value').each(function(i, node) {
              backgrounds.push(templateData.backgroundimages[$(node).text()].filename);
          });

          result.landscapeimage = backgrounds;
        } else if (child.getAttribute('id') === "row-text") {

            // Check for row of text
            $child.find('> values > value').each(function(i, node) {
                rowTexts.push(templateData.rowTexts[$(node).text()]);
            });

            result.rowTexts = rowTexts;

        } else if (child.getAttribute('id') === "row-events") {
            // Check for row of events
            $child.find('> values > value').each(function(i, node) {
                rowEvents.push(templateData.rowEvents[$(node).text()]);
            });

            result.rowEvents = rowEvents;
        } else if (child.getAttribute('id') === "row-media") {
            // Check for row of events
            $child.find('> values > value').each(function(i, node) {
                rowMedias.push(templateData.rowMedias[$(node).text()]);
            });

            result.rowMedias = rowMedias;
        } else if (child.getAttribute('value')) {
          result[child.getAttribute('id')] = child.getAttribute('value');
        }
    });

    result.type = type;
    result.sectionid = sectionId;

    if (!templateData[type]) {
      templateData[type] = {};
    }

    templateData[type][sectionId] = result;

    return result;
}

function processLayout(layout, config, layoutId, sectionId) {
    // TODO make this more dynamic, right now it's hardcoded for 2 containers
    var layoutContainers;

    layout = layout || config.defaults.layout;
    layoutContainers = $.extend({}, config.layouts[layout]);

    layoutContainers.container1 = sectionId;
    layoutContainers.control = sectionId;

    if (!templateData['layout-' + layout]) {
      templateData['layout-' + layout] = {};
    }
    templateData['layout-' + layout][layoutId] = layoutContainers;

    return {
      type: 'layout-two-containers'
    };
}

function processXML(data) {
    var $xml          = $(data),
        deferred      = Q.defer();

    getConfigFile(data, function(jsonConfig) {
        templateData.config = jsonConfig.fields.config;
        
        if (jsonConfig.templateUrls) {
          templateData.config.externalasseturl = jsonConfig.templateUrls.assetsPath;
        }

        templateData.config.seriesId = $xml.find('template #appConfig #seriesID').attr('value');
        templateData.config.eventAnimation = $xml.find('template #appConfig #slide-transition').attr('value');

        if (Nanonation.remote) {
          templateData.config.asseturl = templateData.config.externalasseturl;
        }
        console.log(111, templateData);

        templateData.backgroundimages = processBackgroundImages($xml);
        templateData.rowTexts = processFixedText($xml);
        templateData.rowEvents = processEvents($xml);
        templateData.rowMedias = processMedia($xml);

        templateData.sections = {};
        //templateData.sections['footer'] = processSection('fixed-text', $xml.find('template > [name="[footer]"]'), jsonConfig.fields['fixed-text'], 'footer');


        templateData.sequence = [];

        // create sections
        $xml.find('template > group').each(function(i, node) {
            var $node = $(node),
                sectionId, sectionType,
                layout, layoutId;

            if ((node.getAttribute('name') && node.getAttribute('name')[0] === '[') || node.getAttribute('id') === 'appConfig') {
              return;
            }

            if ($node.find('> [id="layout"]').length === 0) {
              layout = jsonConfig.defaults.layout;
            }

            sectionId = genId();
            layoutId = genId();

            sectionType = node.getAttribute('id');
            templateData.sections[sectionId] = processSection(sectionType, $node, jsonConfig.fields[sectionType], sectionId);

            templateData.sections[layoutId] = processLayout(templateData.sections[sectionId].layout, jsonConfig, layoutId, sectionId);
            templateData.sections[layoutId].sectionid = layoutId;


            templateData.sequence.push({
              sectionid: layoutId,
              transition: $xml.find('template #appConfig #slide-transition').attr('value')
            });
        });

        deferred.resolve(templateData);
    });

    return deferred.promise;
}

var Nanonation = {
  requestData: function() {
      var source    = utils.getUrlParameter('s'),
          _self     = this,
          deferred  = Q.defer();

      templateData = {};

      this.source = source;
      console.log('config:', clientConfig);
      $.ajax({
        url: this.remote ? clientConfig.mycityApiUrl + 'nanonation/template/' + source + '.xml' : clientConfig.nanonationSrc + source + '.xml',
        dataType: 'xml',
        beforeSend: function(xhr) {
            if (_self.remote) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(clientConfig.mycityApiUser + ":" + clientConfig.mycityApiPass));
            }
        }
      })
        .done(function(data) {
            deferred.resolve(data);
        });

      return deferred.promise;
  },

  getData: function() {
      var self      = this,
          deferred  = Q.defer(),
          _self     = this,
          sourceUrl;
          
      this.remote = utils.getUrlParameter("remote");
      
      if (this.data) {
        return this.data;

      } else {
        this.requestData().then(processXML).then(function(data) {
            _self.data = data;
            console.log('template data:', data);
            deferred.resolve(data);
        });
      }

      return deferred.promise;
  }
};

module.exports = Nanonation;