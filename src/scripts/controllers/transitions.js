var templates  = require('../templates').src;

var Transitions = {
  $els: {},

  play: function(transition) {
      // check if we need to render the transition html

      if (transition === 'non-animate') {
          return;
      }

      if (transition === 'animate') {
          $('body').addClass('swype');

          //return;

          setTimeout(function() {
              $('body').addClass('in');
          }, 1000);


          setTimeout(function() {
              if (transition === 'animate') {
                  $('.location-swipe').hide().removeClass('animate');
              }

              // $('body').removeClass('type-' + transition);
              $('body').addClass('out');
          }, 2750);

          setTimeout(function() {
              // $('body').removeClass('type-' + transition);
              $('body').removeClass('in');
              $('body').removeClass('out');
          }, 4250);
      } else {
          setTimeout(function() {
              $('body').addClass('play-' + transition);
          }, 0);

          setTimeout(function() {
              // $('body').removeClass('type-' + transition);
              $('body').removeClass('play-' + transition);
          }, 2750);
      }

  }
};

module.exports = Transitions;