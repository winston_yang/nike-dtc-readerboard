var Sections    = require('../sections'),
    Nanonation  = require('../data/nanonation'),
    templates   = require('../templates').src;

var Slide = {
  defaultScrollDuration: 4500,
  loopCycle: {},
  currentEvent: 0,
  currentEventStep: 0,
  totalEventStep: 3,
  MAX_EVENT: 3,
  endOfEventLoop: false,
  intvEventStep: null,
  eventDisplayAnimationDelta : 400,

  render: function(id) {
      var sectionData,
          content = {},
          childrenItem = [],
          Sections = require('../sections'),
          Calendar = require('./calendar'),
          nanonationData = Nanonation.getData(),
          $el, i, rows = {}, rowText,_textStyle, eventList = [], self = this;


      sectionData = Sections.getSectionData(id);

      if (!sectionData) {
          return false;
      }

      if (sectionData.rowEvents.length) {
          for (i = 0; i < Calendar.events.length; i++) {

              if ((sectionData.rowEvents[0].eventType === 'NRC' && Calendar.events[i].type.id === 2) || (sectionData.rowEvents[0].eventType === 'NTC' && Calendar.events[i].type.id === 3))
              {
                  eventList.push(Calendar.events[i]);
                  if (eventList.length === self.MAX_EVENT) {
                      break;
                  }
              }

          }

          for (i = 0; i < sectionData.rowEvents.length; i++) {
              sectionData.rowEvents[i].events = eventList;

              _textStyle = '';
              if (sectionData.rowEvents[i].fontSize) {
                  _textStyle += 'font-size:' + sectionData.rowEvents[i].fontSize + 'px;';
              }
              if (sectionData.rowEvents[i].color) {
                  _textStyle += 'color:' + sectionData.rowEvents[i].color + ';';
              }
              sectionData.rowEvents[i].textStyle = _textStyle;

              childrenItem.push(sectionData.rowEvents[i]);
          }
      }

      if (sectionData.rowTexts) {
          for (i = 0; i < sectionData.rowTexts.length; i++) {
              rowText = sectionData.rowTexts[i];
              _textStyle = '';
              if (rowText.fontSize) {
                  _textStyle += 'font-size:' + rowText.fontSize + 'px;';
              }
              if (rowText.color) {
                  _textStyle += 'color:' + rowText.color + ';';
              }
              rowText.textStyle = _textStyle;
              childrenItem.push(rowText);
          }
      }

      if (sectionData.rowMedias) {
          for (i = 0; i < sectionData.rowMedias.length; i++) {
              if (nanonationData.config.asseturl && sectionData.rowMedias[i].filename.indexOf('http') === -1) {
                  sectionData.rowMedias[i].filename = location.protocol + nanonationData.config.asseturl + sectionData.rowMedias[i].filename;
              }
              childrenItem.push(sectionData.rowMedias[i]);
          }
      }

      childrenItem.sort(function(a, b){
          return a.rowOrder-b.rowOrder;
      });

      if (sectionData.landscapeimage && sectionData.landscapeimage.length) {
          content.backgroundImage = sectionData.landscapeimage[0];
      }

      $el = $(sectionData.template({
          content: content,
          sectionid: sectionData.sectionid
      }));

      for (i = 0; i < childrenItem.length; i++) {
          $el.append(templates[childrenItem[i].type]({
              content: childrenItem[i]
          }));
      }

      for (i = 1; i <= nanonationData.config.numberOfRows; i++) {
          if ($el.find('.slide__row-order-' + i).length > 1) {
              $el.find('.slide__row-order-' + i).addClass('slide__row-multiple');
              this.loopCycle[i] = 0;
          }
      }

      return $el;
  },

  animateEvents: function(id) {
      var Sections        = require('../sections'),
          $el             = Sections.getEl(id),
          self            = this,
          sectionData     = Sections.getSectionData(id),
          nanonationData  = Nanonation.getData();
          totalEvents     = sectionData.rowEvents[0].events.length-1,
          scrollDuration  = sectionData.rowEvents[0].scrollDuration || this.defaultScrollDuration;

      if ($el === undefined) {
          return;
      }

      if (self.endOfEventLoop && nanonationData.config.eventAnimation === 'non-animate') {
        $el.trigger('endanimation', [id]);
        return;
      }

      $el.find('.event-list-item').hide();
      $el.find('.event-static:eq(' + self.currentEvent + ')').find('.event-list-item:eq(' + self.currentEventStep + ')').show();

      //animate events for static version
      if (self.currentEvent === totalEvents && self.currentEventStep === self.totalEventStep) {
        //reach last event and event step
        self.currentEvent = 0;
        self.currentEventStep = 0;

        self.endOfEventLoop = true;

      } else if (self.currentEventStep === self.totalEventStep) {
        //reach last event step, go to next event
        self.currentEvent++;
        self.currentEventStep = 0;

      } else {
        if (self.currentEventStep < self.totalEventStep) {
          //next event step
          self.currentEventStep++;
        }
      }

      self.intvEventStep = setTimeout(function() {
          self.animateEvents(id);
      }, scrollDuration);
  },

  // nextSection: function(id) {
  //     var Sections        = require('../sections'),
  //         $el             = Sections.getEl(id),
  //         nanonationData  = Nanonation.getData();
  //         self            = this;

  //     if (!self.endOfEventLoop && nanonationData.config.eventAnimation === 'non-animate') {
  //         setTimeout(function() {
  //             self.nextSection(id);
  //         }, 100);

  //         return;
  //     }

  //     $el.trigger('endanimation', [id]);
  // },

    animateLocation: function(el) {
        var self = this;

        function animateItem($item, delay) {
            var $swipeEl = $item.find('.location-swipe');

            setTimeout(function() {
                $swipeEl.show();

                setTimeout(function() {
                    $swipeEl.addClass('animate');
                }, 20);
            }, delay);
        }

//        $(document.body).removeClass('.event-slide').removeClass('out');
//        el.addClass('in');

        var $eventItems = el.find('.event-slide'),
            animDelay = self.defaultScrollDuration / 2 - 1000,
            $el;

        for (var i = 0; i < $eventItems.length; ++i) {
            $el = $($eventItems[i]);
            animateItem($el, animDelay);
            animDelay += self.eventDisplayAnimationDelta;
        }
    },
  //refresh
  animate: function(id, duration, preventTrigger) {
      var Sections        = require('../sections'),
          $el             = Sections.getEl(id),
          sectionData     = Sections.getSectionData(id),
          self            = this,
          nanonationData  = Nanonation.getData();

      duration = duration || this.defaultScrollDuration;

      self.defaultScrollDuration = duration;

      console.log('animate');
      //alternate rows that has same row number.
      for (i = 1; i <= nanonationData.config.numberOfRows; i++) {
        var target = $el.find('.slide__row-order-' + i);

        if (target.length > 1) {
            target.removeClass('active');
            $(target[this.loopCycle[i]]).addClass('active');
            this.loopCycle[i]++;
            if (this.loopCycle[i] >= target.length) {
                this.loopCycle[i] = 0;
            }
        }
      }

      if (nanonationData.config.eventAnimation === 'non-animate' && sectionData.rowEvents[0] && sectionData.rowEvents[0].events.length) {
        self.currentEvent = 0;
        self.currentEventStep = 0;
        self.endOfEventLoop = false;
        clearTimeout(self.intvEventStep);
        self.animateEvents(id);
      
      } else if (nanonationData.config.eventAnimation === 'non-animate' && sectionData.rowEvents[0] && sectionData.rowEvents[0].events && !sectionData.rowEvents[0].length) {
        this.endOfEventLoop = true;

        if (!preventTrigger) {
          setTimeout(function() {
              self.animateEvents(id);
          }, duration);
        }
      } else if (nanonationData.config.eventAnimation === 'animate') {
          if (!preventTrigger) {
              var $el = Sections.getEl(id);

              setTimeout(function() {
                  $el.trigger('endanimation', [id]);
              }, duration);


              self.animateLocation($el);

          }
      }
  }
};

module.exports = Slide;