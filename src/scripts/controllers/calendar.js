var Service       = require('../modules/service'),
    Nanonation    = require('../data/nanonation'),
    clientConfig  = require('../client'),
    moment        = require('moment');

var Calendar = {
  hourOffset: moment().zone() / 60,

  formatFriendlyDateTime: function(events) {
      var shortDays = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
          eventDate,
          day,
          time,
          dateMoment;

      if (!events) {
        return;
      }
      
      for (var i = 0, len = events.length; i < len; ++i) {
        eventDate = new Date(events[i].startDate);
        eventDate.setHours(eventDate.getHours() + this.hourOffset);
        day = shortDays[eventDate.getDay()] + ' ' + (eventDate.getMonth() + 1) + '.' + eventDate.getDate();
        dateMoment = new moment(eventDate);
        time = dateMoment.format(dateMoment.minute() === 0 ? 'hA' : 'h:mmA');

        events[i].friendlyDateTime = day + ' - ' + time;
      }
  },

  setData: function(cb) {
      var self = this,
          data = Nanonation.getData(),
          updateTime;

      function requestCallback(data) {console.log(data);
          self.formatFriendlyDateTime(data.events);

          self.events = data.events;

          self.formatFriendlyDateTime(self.events);

          if (cb) {
            cb();
          }
      }

      Service.requestCalendar({
          url: clientConfig.eventCalendarUrl,
          calendarsources: data.config.seriesId,
          from: moment().format('YYYY-MM-DD'),
          to: moment().add("days",8).format('YYYY-MM-DD')
      }, requestCallback);
  }
};

module.exports = Calendar;