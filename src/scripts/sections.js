var Nanonation = require('./data/nanonation'),
    utils      = require('./utils'),
    templates  = require('./templates').src;

var Sections = {
  controllers: {
    calendar: require('./controllers/calendar'),
    layoutTwoContainers: require('./controllers/layoutTwoContainers'),
    slide: require('./controllers/slide')
  },

  dataCache: {},

  init: function() {
      this.$sectionsContainer = $('.sections-container');
  },

  getSectionData: function(id) {
      var nanonationData = Nanonation.getData(),
          sectionConfig = nanonationData.sections[id],
          sectionData,
          template,
          data;

      if (this.dataCache[id]) {
        return this.dataCache[id];
      }

      sectionConfig = nanonationData.sections[id];

      if (!sectionConfig) {
        return false;
      }

      sectionData = nanonationData[sectionConfig.type][id];
      template = templates[sectionConfig.type];

      data = {};

      utils.extend(sectionConfig, data, false);
      utils.extend(sectionData, data, false);
      data.template = template;

      data.jsController = this.controllers[utils.toCamelCase(sectionConfig.type)];

      this.dataCache[id] = data;

      return data;
  },

  // create section El and assign to data object cache
  create: function(id) {
      var data = this.getSectionData(id);

      if (this.getEl(id)) {
        this.destroyEl(id);
      }

      this.$sectionsContainer.append( this.render(id) );
  },

  getEl: function(id) {
      var data = this.getSectionData(id);
      
      if (!data) {
        return false;
      }

      return data.$el;
  },

  // generate the section el markup
  render: function(id) {
      var data = this.getSectionData(id);

      if (!data.$el) {
        data.$el = data.jsController.render(id);
        this.setSectionBackground(id);

        return data.$el;
      
      } else {
        return data.$el.clone();
      }
  },

  setSectionBackground: function(id) {
      var data = this.getSectionData(id),
          isPortrait = $(window).height() > $(window).width();

      if (isPortrait) {
        if (data.portraitimage) {
          data.imagebackground = data.portraitimage;
        }
        if (data.portraitvideo) {
          data.videobackground = data.portraitvideo;
        }
      
      } else {
        if (data.landscapeimage) {
          data.imagebackground = data.landscapeimage;
        }
        if (data.landscapevideo) {
          data.videobackground = data.landscapevideo;
        }
      }
      
      if (data.videobackground) {
        this.setVideoBackground(data);
      }
  },

  setVideoBackground: function(sectionData) {
      var videoUrl,
          $video;

      sectionData.video = {
        els: []
      };

      for (var i = 0, len = sectionData.videobackground.length; i < len; ++i) {
        if (PS.data.config.asseturl) {
          videoUrl = PS.data.config.asseturl + sectionData.videobackground[i];

        } else if (sectionData.videobackground[i].indexOf('http') === -1) {
          videoUrl = '../assets/videos/' + sectionData.videobackground[i];
        
        } else {
          videoUrl = sectionData.videobackground[i];
        }

        $video = $('<video muted preload="auto" class="video-background"><source src="' + videoUrl + '" /></video>');
        
        sectionData.$el.prepend($video);
        sectionData.video.els.push({$video: $video});
      }
  },

  show: function(section) {
      var data = this.getSectionData(section.sectionid);

      data.$el.removeClass('hide');
      data.$el.addClass('show');

      if (data.video) {
        this.runVideoBackground(data, function() {
            data.jsController.animate(data.sectionid, data.video.els[data.video.videoIndex].duration);
        });

      } else {
        this.setImageBackground(data);
        data.jsController.animate(data.sectionid, section.duration);
      }
  },

  hide: function(id) {
      var sectionData   = this.getSectionData(id),
          targetSection = this.getEl(id);

      targetSection.addClass('hide');
      targetSection.removeClass('show');


      setTimeout(function() {
          targetSection.removeClass('hide');
      }, 1000);

      if (sectionData.video) {
        this.stopVideoBackground(sectionData);
      }
  },

  setImageBackground: function(sectionData) {
      var imageIndex = typeof sectionData.backgroundIndex !== "undefined" ? sectionData.backgroundIndex + 1 : 0,
          imgageUrl = '',
          $el = sectionData.$el,
          nanonationData = Nanonation.getData();

      if (!sectionData.imagebackground || !sectionData.imagebackground.length) {
        return;
      }

      if (imageIndex === sectionData.imagebackground.length) {
        imageIndex = 0;
      }

      imgageUrl = nanonationData.config.asseturl + sectionData.imagebackground[imageIndex];

      $el.css('background-image', 'url(' + imgageUrl + ')');
      $el.addClass('image-background');

      sectionData.backgroundIndex = imageIndex;
  },
};

module.exports = Sections;