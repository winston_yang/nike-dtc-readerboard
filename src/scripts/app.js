var $ = require('jquery'),
    SequenceManager = require('./SequenceManager');

window.jQuery = window.$ = $;

$(function() {
    SequenceManager.init();
});