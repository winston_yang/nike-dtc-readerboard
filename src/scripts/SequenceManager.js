var Nanonation  = require('./data/nanonation'),
    Sections    = require('./sections'),
    transitions = require('./controllers/transitions'),
    templates  = require('./templates/').src;

var SequenceManager = {
  currentSectionIdx: 0,
  previousSectionIdx: 0,
  refreshExperience: false,
  loopCount: 1,

  init: function() {
      var _self = this;

      console.log('Sequence Manager init');

      Nanonation.getData().then(function(data) {
          $('head').append('<link rel="stylesheet" href="' + MyCity.skinUrl + data.config.eventAnimation + '/theme.css" type="text/css" />');
          _self.setSequenceData(data);
      });
  },

  setSequenceData: function(data) {
      var _self = this,
          sections = data.sections,
          dataRequestsCount = 0,
          dataRequestsComplete = 0,
          sectionData;

      function checkDataRequests() {
          var delay = 0;

          dataRequestsComplete++;

          if (dataRequestsCount === dataRequestsComplete) {

              transition = _self.sequence[_self.currentSectionIdx].transition || "multi-swipe";

              if (transition === 'animate') {
                  $('body').append($(templates['transition-' + transition]()));
                  transitions.play(transition);
                  delay =1600;
              }

              setTimeout(function() {
                  _self.renderSections();

                  setTimeout(function() {
                      _self.startSequence();
                  }, 100);
              }, delay);


          }
      }

      this.sequence = data.sequence;
      Sections.init();

      for (var sectionId in sections) {
        sectionData = Sections.getSectionData(sectionId);
        
        if (sectionData.jsController.setData) {
          dataRequestsCount++;
          sectionData.jsController.setData(sectionId, checkDataRequests);
        }
      }

      dataRequestsCount++;

      Sections.controllers.calendar.setData(checkDataRequests);

      if (dataRequestsCount === 0) {
        dataRequestsCount = 1;
        checkDataRequests();
      }
  },

  renderSections: function() {
      var sequence = this.sequence;

      for (var i = 0, len = sequence.length; i < len; ++i) {
        Sections.create(sequence[i].sectionid);
      }
  },

  startSequence: function() {
      var self = this;

      Sections.show(this.sequence[0]);

      Sections.$sectionsContainer.on('endanimation', function(ev, id) {
          self.playTransition(id);
      });
  },

  playTransition: function(id) {
      var transition = this.sequence[this.currentSectionIdx].transition || "multi-swipe",
          self = this, delay = 800;

      transitions.play(transition);

      if (transition === 'animate') {
          delay = 1600;
      }
      setTimeout(function() {
          self.showNext();
      }, delay);
  },

  showNext: function() {
      this.previousSectionIdx = this.currentSectionIdx;

      this.getNewCurrentIndex();

      // refresh browser
      if (this.refreshExperience && this.currentSectionIdx === 0) {
          location.reload();
        
        // } else {
        //   this.resetSequence();
        // }

      } else {
        Sections.hide(this.sequence[this.previousSectionIdx].sectionid);

        Sections.show(this.sequence[this.currentSectionIdx]);
      }

      if (this.currentSectionIdx === 0) {
        this.loopCount++;
      }
  },

  getNewCurrentIndex: function() {
      var sectionData;

      this.currentSectionIdx = this.currentSectionIdx === this.sequence.length - 1 ? 0 : this.currentSectionIdx + 1;

      if (this.sequence[this.currentSectionIdx].loopcycle &&
          (this.loopCount % this.sequence[this.currentSectionIdx].loopcycle) !== 0) {

        this.currentSectionIdx = this.currentSectionIdx + 1 === this.sequence.length ? 0 : this.currentSectionIdx + 1;
      }
      
      sectionData = Sections.getSectionData(this.sequence[this.currentSectionIdx].sectionid);

      if (sectionData.jsController.canPlay && !sectionData.jsController.canPlay(sectionData.sectionid)) {
        this.getNewCurrentIndex();
      }
  },
};

module.exports = SequenceManager;