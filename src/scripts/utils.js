var Utils = {
  getUrlParameter: function(sParam) {
      var sPageURL = window.location.search.substring(1),
          sURLVariables = sPageURL.split('&');

      for (var i = 0; i < sURLVariables.length; i++) {
          var sParameterName = sURLVariables[i].split('=');
          if (sParameterName[0] == sParam) {
            return sParameterName[1];
          }
      }
  },

  extend: function(src, obj, moduleExtension) {
      if (moduleExtension) {
        obj._super = src;
      }

      for (var prop in src) {
        if (src.hasOwnProperty(prop) && !obj.hasOwnProperty(prop)) {
          obj[prop] = src[prop];
        }
      }
      return obj;
      // return Object.create(obj);
  },

  toCamelCase: function(str) {
      return str.replace(/-([a-z])/g, function (g) {
          return g[1].toUpperCase();
      });
  }  
};

module.exports = Utils;