#!/bin/bash
#
#build script for Fuel Pledge
#params:
#    environment ( teamsite-dev | teamsite-int ) - required
#    teamsite username ( <username> )            - required
#    kick of grunt build ( build )               - optional

if [ "$1" == "" ]
then
    echo 'pass environment as arg 1 (development | production)'
    echo '##########'
    echo    'help:'
    echo    'environment ( development | production) - required'

    exit
fi

export NODE_ENV=$1

if [ "$2" == "--set-latest" ]
then 
    echo "deploying $NODE_ENV assets, setting latest"
    gulp deploy-assets --set-latest

else
    
    echo "deploying $NODE_ENV assets"

    gulp deploy-assets
fi

exit