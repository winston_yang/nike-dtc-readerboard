#!/bin/bash
#
#build script for Fuel Pledge
#params:
#    environment ( teamsite-dev | teamsite-int ) - required
#    teamsite username ( <username> )            - required
#    kick of grunt build ( build )               - optional

if [ "$1" == "" ]
then
    echo 'pass environment as arg 1 (development-aws | production)'
    echo '##########'
    echo    'help:'
    echo    'environment ( development | production) - required'

    exit
fi

if [ "$2" != "" ]
then 
    echo "deploying $1 from $2"

    gulp update-from-eb -s $2 -d $1

else
    export NODE_ENV=$1

    echo "deploying $NODE_ENV"

    gulp update-eb
fi

exit